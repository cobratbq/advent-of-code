module github.com/cobratbq/adventofcode

go 1.22

require github.com/cobratbq/goutils v0.0.0-20241128174720-62fc8009cf64

require (
	github.com/draffensperger/golp v0.0.0-20220125165800-1a05e30b4363
	github.com/stretchr/testify v1.8.2 // indirect
)

replace github.com/cobratbq/goutils => ../goutils
