package main

import (
	"regexp"

	"github.com/cobratbq/goutils/assert"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	"github.com/cobratbq/goutils/std/builtin"
	"github.com/cobratbq/goutils/std/builtin/maps"
	io_ "github.com/cobratbq/goutils/std/io"
	"github.com/cobratbq/goutils/std/log"
	math_ "github.com/cobratbq/goutils/std/math"
	sort_ "github.com/cobratbq/goutils/std/sort"
	"github.com/cobratbq/goutils/std/strconv"
)

func main() {
	sensors := builtin.Expect(readInput("input.txt"))
	log.Debugf("Sensors: %+v", sensors)
	intervals := deriveIntervalMap(sensors)
	intervalcount := maps.FoldValues(intervals, 0, func(count int, intvs []interval) int {
		return count + len(intvs)
	})
	log.Debugln(len(intervals), intervalcount)
	maps.UpdateValue(intervals, func(row int, value []interval) []interval {
		sort_.Slice(value, func(i, j int) bool { return less(&value[i], &value[j]) })
		var joined []interval
	next_interval:
		for _, intv := range value {
			for idx, join := range joined {
				if intv.from <= join.to {
					joined[idx] = interval{from: join.from, to: math_.Max(join.to, intv.to)}
					continue next_interval
				}
			}
			joined = append(joined, intv)
		}
		if len(joined) > 1 {
			log.Debugln("Multiple intervals:", row, len(joined), joined)
		}
		return joined
	})
	joinedcount := maps.FoldValues(intervals, 0, func(counter int, intervals []interval) int {
		return counter + len(intervals)
	})
	log.Debugln(len(intervals), joinedcount, intervals[0], intervals[MAX])
	for y, intv := range intervals {
		if len(intv) != 1 {
			log.Debugln("Unexpected number of intervals:", y, intv)
		}
	}
}

func deriveIntervalMap(sensors []sensor) map[int][]interval {
	intervals := make(map[int][]interval)
	for _, s := range sensors {
		for relY := -s.reach; relY <= s.reach; relY++ {
			rowreach := s.reach - math_.AbsInt(relY)
			absY := s.position.y + relY
			if absY < 0 || absY > MAX {
				continue
			}
			intervals[absY] = append(intervals[absY], interval{
				from: math_.Max(0, s.position.x-rowreach),
				to:   math_.Min(MAX, s.position.x+rowreach),
			})
		}
	}
	return intervals
}

const MAX = 4_000_000

func less(a, b *interval) bool {
	return a.from < b.from || (a.from == b.from && a.to < b.to)
}

type interval struct {
	from int
	to   int
}

func readInput(name string) ([]sensor, error) {
	reader, closer, inputErr := bufio_.OpenFileReadOnly(name)
	assert.Success(inputErr, "Failed to open input file")
	defer io_.ClosePanicked(closer, "Failed to close input file")
	return bufio_.ReadProcessBytesLinesFunc(reader, '\n', func(line []byte) (sensor, error) {
		if len(line) == 0 {
			return sensor{}, bufio_.ErrProcessingIgnore
		}
		match := SensorReadingPattern.FindSubmatch(line)
		assert.Required(match, "Invalid line: "+string(line))
		var s sensor
		s.position.x = strconv.MustParseInt[int](string(match[1]), 10)
		s.position.y = strconv.MustParseInt[int](string(match[2]), 10)
		s.closest.x = strconv.MustParseInt[int](string(match[3]), 10)
		s.closest.y = strconv.MustParseInt[int](string(match[4]), 10)
		s.reach = math_.Difference(s.position.x, s.closest.x) +
			math_.Difference(s.position.y, s.closest.y)
		return s, nil
	})
}

var SensorReadingPattern = regexp.MustCompile(`^Sensor at x=(-?\d+), y=(-?\d+): closest beacon is at x=(-?\d+), y=(-?\d+)$`)

type sensor struct {
	position coord
	closest  coord
	reach    int
}

type coord struct {
	x int
	y int
}
