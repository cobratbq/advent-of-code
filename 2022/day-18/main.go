package main

import (
	"strings"

	"github.com/cobratbq/goutils/assert"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	"github.com/cobratbq/goutils/std/builtin"
	"github.com/cobratbq/goutils/std/builtin/maps"
	"github.com/cobratbq/goutils/std/builtin/set"
	"github.com/cobratbq/goutils/std/builtin/slices"
	io_ "github.com/cobratbq/goutils/std/io"
	"github.com/cobratbq/goutils/std/log"
	"github.com/cobratbq/goutils/std/strconv"
)

func main() {
	input := builtin.Expect(readInput("input.txt"))
	lava, sides := countSides(input)
	log.Infoln("Sides:", sides)
	max := findMax(input)
	// occupied are cells occupied by exterior or lava
	occupied := floodFill(lava, max)
	log.Infoln("Occupied:", len(occupied))
	airpockets := complement(occupied, max)
	_, pocketsides := countSides(airpockets)
	log.Infoln("Airpocket-sides:", pocketsides)
	surface := sides - pocketsides
	log.Infoln("Surface:", surface)
}

func complement(occupied map[voxel]struct{}, max voxel) []voxel {
	var compl []voxel
	for x := 0; x < max.x; x++ {
		for y := 0; y < max.y; y++ {
			for z := 0; z < max.z; z++ {
				cell := voxel{x, y, z}
				if _, ok := occupied[cell]; !ok {
					compl = append(compl, cell)
				}
			}
		}
	}
	log.Debugln("Complement:", len(compl))
	return compl
}

func floodFill(lava map[voxel]struct{}, max voxel) map[voxel]struct{} {
	log.Debugln("Flooding with boundary:", max)
	exterior := maps.Duplicate(lava)
	todo := []*voxel{{x: 0, y: 0, z: 0}}
	var next *voxel
	for i := 0; i < len(todo); i++ {
		c := todo[i]
		set.Insert(exterior, *c)
		next = &voxel{x: c.x - 1, y: c.y, z: c.z}
		if !set.Contains(exterior, *next) && next.x >= 0 {
			set.Insert(exterior, *next)
			todo = append(todo, next)
		}
		next = &voxel{x: c.x + 1, y: c.y, z: c.z}
		if !set.Contains(exterior, *next) && next.x < max.x {
			set.Insert(exterior, *next)
			todo = append(todo, next)
		}
		next = &voxel{x: c.x, y: c.y - 1, z: c.z}
		if !set.Contains(exterior, *next) && next.y >= 0 {
			set.Insert(exterior, *next)
			todo = append(todo, next)
		}
		next = &voxel{x: c.x, y: c.y + 1, z: c.z}
		if !set.Contains(exterior, *next) && next.y < max.y {
			set.Insert(exterior, *next)
			todo = append(todo, next)
		}
		next = &voxel{x: c.x, y: c.y, z: c.z - 1}
		if !set.Contains(exterior, *next) && next.z >= 0 {
			set.Insert(exterior, *next)
			todo = append(todo, next)
		}
		next = &voxel{x: c.x, y: c.y, z: c.z + 1}
		if !set.Contains(exterior, *next) && next.z < max.z {
			set.Insert(exterior, *next)
			todo = append(todo, next)
		}
	}
	return exterior
}

func findMax(input []voxel) voxel {
	max := slices.Fold(input, voxel{0, 0, 0}, func(r voxel, v voxel) voxel {
		if v.x > r.x {
			r.x = v.x
		}
		if v.y > r.y {
			r.y = v.y
		}
		if v.z > r.z {
			r.z = v.z
		}
		return r
	})
	// assuming that it is more convenient to add 1 to the max boundary for the flood-filling to
	// reach everywhere. This is based on a hunch, not a previous failure, so may result in
	// excessive effort later.
	max.x += 1
	max.y += 1
	max.z += 1
	return max
}

func countSides(input []voxel) (map[voxel]struct{}, uint) {
	lava := make(map[voxel]struct{})
	sides := slices.Fold(input, 0, func(s uint, v voxel) uint {
		newSides := uint(6)
		if v.x > 0 && set.Contains(lava, voxel{x: v.x - 1, y: v.y, z: v.z}) {
			s--
			newSides--
		}
		if set.Contains(lava, voxel{x: v.x + 1, y: v.y, z: v.z}) {
			s--
			newSides--
		}
		if v.y > 0 && set.Contains(lava, voxel{x: v.x, y: v.y - 1, z: v.z}) {
			s--
			newSides--
		}
		if set.Contains(lava, voxel{x: v.x, y: v.y + 1, z: v.z}) {
			s--
			newSides--
		}
		if v.z > 0 && set.Contains(lava, voxel{x: v.x, y: v.y, z: v.z - 1}) {
			s--
			newSides--
		}
		if set.Contains(lava, voxel{x: v.x, y: v.y, z: v.z + 1}) {
			s--
			newSides--
		}
		set.Insert(lava, v)
		return s + newSides
	})
	assert.Equal(len(input), len(lava))
	return lava, sides
}

func readInput(name string) ([]voxel, error) {
	reader, closer, inputErr := bufio_.OpenFileReadOnly(name)
	assert.Success(inputErr, "Failed to open input file")
	defer io_.ClosePanicked(closer, "Failed to close input file")
	return bufio_.ReadProcessStringLinesFunc(reader, '\n', func(line string) (voxel, error) {
		if line == "" {
			return voxel{}, bufio_.ErrProcessingIgnore
		}
		components := strings.Split(line, ",")
		assert.Equal(3, len(components))
		return voxel{
			x: strconv.MustParseInt[int](components[0], 10),
			y: strconv.MustParseInt[int](components[1], 10),
			z: strconv.MustParseInt[int](components[2], 10),
		}, nil
	})
}

type voxel struct{ x, y, z int }
