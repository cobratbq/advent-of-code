package main

import (
	"strconv"

	"github.com/cobratbq/goutils/assert"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	"github.com/cobratbq/goutils/std/builtin"
	"github.com/cobratbq/goutils/std/builtin/slices"
	io_ "github.com/cobratbq/goutils/std/io"
	"github.com/cobratbq/goutils/std/log"
	"github.com/cobratbq/goutils/types"
)

func main() {
	input := builtin.Expect(readInput("input.txt"))
	slices.Update(input, func(v int) int { return v * 811589153 })
	src := slices.Transform(input, func(e int) *int { return &e })
	dest := slices.Duplicate(src)
	mod := len(src)
	for i := 0; i < 10; i++ {
		for _, p := range src {
			from := slices.Index(dest, p)
			assert.Require(from >= 0, "BUG: failed to find element")
			to := (from + *p) % (mod - 1)
			if to < 0 {
				to += mod - 1
			}
			//log.Debugln("Element", *p, "source", from, "dest", to)
			slices.MoveElementTo(dest, from, to)
		}
		//print(dest)
	}
	zeroidx := slices.IndexFunc(dest, func(p *int) bool { return *p == 0 })
	log.Debugln("Zero-index:", zeroidx, *dest[zeroidx],
		"sum:", *dest[(zeroidx+1000)%mod]+*dest[(zeroidx+2000)%mod]+*dest[(zeroidx+3000)%mod],
		"values", *dest[(zeroidx+1000)%mod], *dest[(zeroidx+2000)%mod], *dest[(zeroidx+3000)%mod])
}

func print(input []*int) {
	log.Debugln(slices.Transform(input, func(p *int) int { return *p }))
}

func readInput(name string) ([]int, error) {
	reader, closer, inputErr := bufio_.OpenFileReadOnly(name)
	assert.Success(inputErr, "Failed to open input file")
	defer io_.ClosePanicked(closer, "Failed to close input file")
	return bufio_.ReadProcessStringLinesFunc(reader, '\n', func(line string) (int, error) {
		if line == "" {
			return 0, bufio_.ErrProcessingIgnore
		}
		value, err := parseLine(line)
		return int(value), err
	})
}

func parseLine(line string) (int64, error) {
	return strconv.ParseInt(line, 10, types.IntSize)
}
