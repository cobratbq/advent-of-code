package main

import (
	"fmt"
	"os"

	"github.com/cobratbq/goutils/assert"
	"github.com/cobratbq/goutils/codec/uint/coord"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	"github.com/cobratbq/goutils/std/builtin"
	"github.com/cobratbq/goutils/std/builtin/set"
	"github.com/cobratbq/goutils/std/errors"
	io_ "github.com/cobratbq/goutils/std/io"
	"github.com/cobratbq/goutils/std/log"
	os_ "github.com/cobratbq/goutils/std/os"
)

func main() {
	lines := readInput("input.txt")
	heightmap := transform(lines)
	startidx, analysisErr := analyze(heightmap)
	if analysisErr == nil {
		pathmap := builtin.Expect(validate(heightmap, startidx))
		clearExcessSegments(heightmap, pathmap)
		log.Infoln("Number of steps:", len(pathmap))
	} else {
		log.Debugln(analysisErr.Error())
	}
	fmt.Println("Colorized heightmap:")
	printColorizedMap(heightmap)
	fmt.Println("\n\nPath-map:")
	printPath(heightmap)
}

func readInput(name string) [][]byte {
	reader, closer, inputErr := bufio_.OpenFileReadOnly(name)
	assert.Success(inputErr, "Failed to open input file")
	defer io_.ClosePanicked(closer, "Failed to close input file")
	lines := make([][]byte, 0)
	bufio_.ReadBytesLinesNoDelimFunc(reader, '\n', func(line []byte) error {
		if len(line) == 0 {
			return nil
		}
		lines = append(lines, line)
		return nil
	})
	return lines
}

func printColorizedMap(m heightmap) {
	mark := func(symbol byte) string {
		return os_.ColorText(os_.ColorGreen, string(symbol))
	}
	for i := 0; i < len(m.tiles); i++ {
		if i%int(m.width) == 0 {
			fmt.Println()
		}
		if m.tiles[i].path == 0 || m.tiles[i].path == '.' {
			os.Stdout.WriteString(string(m.tiles[i].level))
		} else {
			os.Stdout.WriteString(mark(m.tiles[i].level))
		}
	}
}

func printPath(m heightmap) {
	for i := 0; i < len(m.tiles); i++ {
		if i%int(m.width) == 0 {
			fmt.Println()
		}
		if m.tiles[i].path == 0 {
			fmt.Print(".")
		} else {
			fmt.Print(string(m.tiles[i].path))
		}
	}
}

func analyze(m heightmap) (uint, error) {
	// NOTE: this function is very bloated due to the amount of debug information. Many lines could
	// be deleted once the function is known to work correctly. However, I leave it in for its
	// traceability.
	bfs := []uint{m.end}
	for i := 0; i < len(bfs); i++ {
		currentidx := bfs[i]
		if m.tiles[currentidx].level == 'S' {
			log.Debugln("Found!!! " + string(m.tiles[currentidx].path))
			return currentidx, nil
		}
		currentTile := m.tiles[currentidx]
		currentX, currentY := coord.Decode2D(m.width, currentidx)
		log.Debugln("Visiting", currentX+1, currentY+1, string(currentTile.level), "(", currentidx, ")")
		if currentX > 0 && walkable(currentTile, m.tiles[coord.Encode2D(m.width, currentX-1, currentY)]) {
			nextX, nextY := currentX-1, currentY
			if left := coord.Encode2D(m.width, nextX, nextY); m.tiles[left].path == 0 {
				log.Debugln("Queuing left neighbor", nextX, nextY, "(", left, ")")
				m.tiles[left].path = '>'
				bfs = append(bfs, left)
			} else {
				log.Debugln("Left neighbor already visited")
			}
		} else {
			log.Debugln("Left neighbor unreachable")
		}
		if currentX < m.width-1 && walkable(currentTile, m.tiles[coord.Encode2D(m.width, currentX+1, currentY)]) {
			nextX, nextY := currentX+1, currentY
			if right := coord.Encode2D(m.width, nextX, nextY); m.tiles[right].path == 0 {
				log.Debugln("Queuing right neighbor", nextX, nextY, "(", right, ")")
				m.tiles[right].path = '<'
				bfs = append(bfs, right)
			} else {
				log.Debugln("Right neighbor already visited")
			}
		} else {
			log.Debugln("Right neighbor unreachable")
		}
		if currentY > 0 && walkable(currentTile, m.tiles[coord.Encode2D(m.width, currentX, currentY-1)]) {
			nextX, nextY := currentX, currentY-1
			if top := coord.Encode2D(m.width, nextX, nextY); m.tiles[top].path == 0 {
				log.Debugln("Queuing top neighbor", nextX, nextY, "(", top, ")")
				m.tiles[top].path = 'v'
				bfs = append(bfs, top)
			} else {
				log.Debugln("Top neighbor already visited")
			}
		} else {
			log.Debugln("Top neighbor unreachable")
		}
		if currentY < m.height-1 && walkable(currentTile, m.tiles[coord.Encode2D(m.width, currentX, currentY+1)]) {
			nextX, nextY := currentX, currentY+1
			if bottom := coord.Encode2D(m.width, nextX, nextY); m.tiles[bottom].path == 0 {
				log.Debugln("Queuing bottom neighbor", nextX, nextY, "(", bottom, ")")
				m.tiles[bottom].path = '^'
				bfs = append(bfs, bottom)
			} else {
				log.Debugln("Bottom neighbor already visited")
			}
		} else {
			log.Debugln("Bottom neighbor unreachable")
		}
		log.Debugln("Remaining entries:", len(bfs[i+1:]), bfs[i+1:])
	}
	return 0, errors.Context(ErrAnalysisFailed, "no more tiles to visit in BFS")
}

var ErrAnalysisFailed = errors.NewStringError("failed to construct path all the way from E to S")

func clearExcessSegments(m heightmap, pathmap map[uint]struct{}) {
	for i := range m.tiles {
		if _, ok := pathmap[uint(i)]; !ok {
			m.tiles[i].path = '.'
		}
	}
	m.tiles[m.start].path = 'S'
	m.tiles[m.end].path = 'E'
}

func validate(m heightmap, startidx uint) (map[uint]struct{}, error) {
	assert.Require(m.tiles[startidx].path != 0,
		"We have not yet constructed a complete path from S to E.")
	pathmap := make(map[uint]struct{}, 0)
	next := startidx
	for next != m.end {
		thisX, thisY := coord.Decode2D(m.width, next)
		switch m.tiles[next].path {
		case '<':
			next = coord.Encode2D(m.width, thisX-1, thisY)
		case '>':
			next = coord.Encode2D(m.width, thisX+1, thisY)
		case '^':
			next = coord.Encode2D(m.width, thisX, thisY-1)
		case 'v':
			next = coord.Encode2D(m.width, thisX, thisY+1)
		default:
			log.Errorln(m.tiles[next].path)
			panic("BUG: should not reach here")
		}
		set.Insert(pathmap, next)
	}
	return pathmap, nil
}

func walkable(current, neighbor tile) bool {
	if current.level == 'E' {
		return neighbor.level == 'z'
	} else if neighbor.level == 'S' {
		return current.level == 'a'
	}
	if neighbor.level < current.level-1 {
		return false
	}
	return true
}

func transform(lines [][]byte) heightmap {
	var m heightmap
	m.height = uint(len(lines))
	m.width = uint(len(lines[0]))
	for _, line := range lines {
		for _, b := range line {
			assert.Equal(m.width, uint(len(line)))
			if b == 'S' {
				m.start = uint(len(m.tiles))
			} else if b == 'E' {
				m.end = uint(len(m.tiles))
			}
			m.tiles = append(m.tiles, tile{level: b})
		}
	}
	assert.Equal('S', m.tiles[m.start].level)
	assert.Equal('E', m.tiles[m.end].level)
	m.tiles[m.end].path = 'E'
	return m
}

type heightmap struct {
	width  uint
	height uint
	start  uint
	end    uint
	tiles  []tile
}

type tile struct {
	level byte
	path  byte
}
