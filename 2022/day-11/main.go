package main

import (
	"regexp"
	"strings"

	"github.com/cobratbq/goutils/assert"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	"github.com/cobratbq/goutils/std/builtin/multiset"
	"github.com/cobratbq/goutils/std/builtin/slices"
	io_ "github.com/cobratbq/goutils/std/io"
	"github.com/cobratbq/goutils/std/log"
	math_ "github.com/cobratbq/goutils/std/math"
	"github.com/cobratbq/goutils/std/strconv"
)

const ROUNDS = 10000

func main() {
	definitions := readInput("input.txt")
	monkeys := slices.Transform(definitions, parseMonkey)
	log.Debugln(monkeys)
	var lcm = determineLCM(divisors)
	log.Debugln("LCM:", lcm)
	var inspections = make(map[int]uint)
	var current uint
	for r := 0; r < ROUNDS; r++ {
		for i := 0; i < len(monkeys); i++ {
			var m = &monkeys[i]
			log.Infoln("Round", r+1, "Monkey", m.id)
			for len(m.items) > 0 {
				current = m.items[0]
				m.items = m.items[1:]
				log.Infoln("  inspecting item with worry level", current)
				// monkey inspects item
				current = m.op(current) % lcm
				multiset.Insert(inspections, m.id)
				log.Infoln("    worry level increases to", current)
				// determine throwing destination
				var dest int
				if m.test.condition(current) {
					dest = m.test.true
				} else {
					dest = m.test.false
				}
				log.Infoln("    monkey throws item with worry level", current, "to monkey", dest)
				monkeys[dest].items = append(monkeys[dest].items, current)
			}
			log.Debugln("Monkey items:", m.items)
		}
		log.Infoln("Round", r+1, "results:")
		for _, m := range monkeys {
			log.Infoln("  Monkey", m.id, ":", m.items)
		}
	}
	log.Infoln("Inspections:")
	for i := 0; i < len(monkeys); i++ {
		log.Infoln("  Monkey", i, ":", inspections[i])
	}
}

// TODO this is slightly nasty code, but it works and it is sufficient/reasonable for the challenge
var divisors []uint

func determineLCM(divs []uint) uint {
	current := divisors[0]
	for i := 1; i < len(divisors); i++ {
		current = math_.LCM(current, divisors[i])
	}
	return current
}

func readInput(name string) []string {
	reader, closer, inputErr := bufio_.OpenFileReadOnly(name)
	assert.Success(inputErr, "Failed to open input file")
	defer io_.ClosePanicked(closer, "Failed to close input file")
	var definitions []string
	var definition string
	readErr := bufio_.ReadStringLinesNoDelimFunc(reader, '\n',
		func(line string) error {
			if line == "" {
				definitions = append(definitions, definition)
				definition = ""
			} else {
				definition += line + "\n"
			}
			return nil
		})
	assert.Success(readErr, "Failed to read lines from input")
	return definitions
}

func parseMonkey(text string) Monkey {
	matches := MonkeyPattern.FindStringSubmatch(text)
	assert.Required(matches, "Failed to match definition from input")
	log.Debugln("Match:", matches)
	var monkey Monkey
	monkey.id = strconv.MustParseInt[int](matches[1], strconv.DecimalBase)
	monkey.items = slices.Transform(strings.Split(matches[2], ", "), func(in string) uint {
		return strconv.MustParseUint[uint](in, strconv.DecimalBase)
	})
	monkey.op = parseOp(matches[3])
	monkey.test.condition = parseTestCond(matches[4])
	monkey.test.true = strconv.MustParseInt[int](matches[5], strconv.DecimalBase)
	monkey.test.false = strconv.MustParseInt[int](matches[6], strconv.DecimalBase)
	return monkey
}

func parseOp(matchOp string) OpFunc {
	if matchOp == "new = old * old" {
		return createSquareOp()
	} else if strings.HasPrefix(matchOp, "new = old * ") {
		return createMultOp(strconv.MustParseUintDecimal[uint](matchOp[12:]))
	} else if strings.HasPrefix(matchOp, "new = old + ") {
		return createAddOp(strconv.MustParseUintDecimal[uint](matchOp[12:]))
	}
	log.Debugf("Unknown operation: %+v", matchOp)
	panic("BUG: unknown operation case encountered")
}

func parseTestCond(testOp string) TestFunc {
	if strings.HasPrefix(testOp, "divisible by ") {
		return createDivisibleByTest(strconv.MustParseUintDecimal[uint](testOp[13:]))
	}
	log.Debugf("Unknown test condition: %+v", testOp)
	panic("BUG: unsupported test condition")
}

var MonkeyPattern = regexp.MustCompile(`^Monkey (\d+):\n  Starting items: (\d+(?:, \d+)*)\n  Operation: ([^\n]+)\n  Test: ([^\n]+)\n    If true: throw to monkey (\d+)\n    If false: throw to monkey (\d+)\n$`)

type Monkey struct {
	id    int
	items []uint
	op    OpFunc
	test  ThrowTest
}

func createAddOp(scalar uint) OpFunc {
	return func(old uint) uint {
		return old + scalar
	}
}

func createMultOp(scalar uint) OpFunc {
	return func(old uint) uint {
		return old * scalar
	}
}

func createSquareOp() OpFunc {
	return func(old uint) uint {
		return old * old
	}
}

type OpFunc func(uint) uint

type ThrowTest struct {
	condition TestFunc
	true      int
	false     int
}

func createDivisibleByTest(scalar uint) TestFunc {
	divisors = append(divisors, scalar)
	return func(value uint) bool {
		return value%scalar == 0
	}
}

type TestFunc func(uint) bool
