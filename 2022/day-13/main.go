package main

import (
	"bytes"
	"fmt"
	"sort"

	"github.com/cobratbq/goutils/assert"
	"github.com/cobratbq/goutils/codec/bytes/digit"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	"github.com/cobratbq/goutils/std/builtin"
	"github.com/cobratbq/goutils/std/builtin/slices"
	io_ "github.com/cobratbq/goutils/std/io"
	"github.com/cobratbq/goutils/std/log"
	"github.com/cobratbq/goutils/std/strconv"
)

func main() {
	reader, closer, inputErr := bufio_.OpenFileReadOnly("input.txt")
	assert.Success(inputErr, "Failed to open input file")
	defer io_.ClosePanicked(closer, "Failed to close input file")
	lines := builtin.Expect(bufio_.ReadProcessBytesLinesFunc(reader, '\n',
		func(line []byte) (interface{}, error) {
			if len(line) == 0 {
				return nil, bufio_.ErrProcessingIgnore
			}
			return parse(line), nil
		}))
	dividerPacket2 := &List{entries: []interface{}{&List{entries: []interface{}{Literal{value: 2}}}}}
	dividerPacket6 := &List{entries: []interface{}{&List{entries: []interface{}{Literal{value: 6}}}}}
	lines = append(lines, dividerPacket2, dividerPacket6)
	sort.Slice(lines, func(i, j int) bool {
		return compare(lines[i], lines[j]) == -1
	})
	for i, p := range lines {
		if bytes.Equal([]byte("[[2]]"), print(p)) {
			log.Infoln("Index for packet 2:", i+1)
		}
		if bytes.Equal([]byte("[[6]]"), print(p)) {
			log.Infoln("Index for packet 6:", i+1)
		}
	}
}

func compare(value1, value2 interface{}) int {
	switch v1 := value1.(type) {
	case *List:
		if v2, ok := value2.(*List); ok {
			return compareLists(v1, v2)
		} else if v2, ok := value2.(Literal); ok {
			return compareLists(v1, &List{entries: []interface{}{v2}})
		} else {
			panic("unknown type encountered for value 2")
		}
	case Literal:
		if v2, ok := value2.(*List); ok {
			return compareLists(&List{entries: []interface{}{v1}}, v2)
		} else if v2, ok := value2.(Literal); ok {
			return compareLiterals(&v1, &v2)
		} else {
			fmt.Printf("%#v\n", v2)
			panic("unknown type encountered for value 2")
		}
	default:
		panic("unknown type encountered for value 1")
	}
}

func compareLists(list1, list2 *List) int {
	for i := 0; i < len(list1.entries); i++ {
		if i >= len(list2.entries) {
			// fewer entries in list 2, this implies that the order is wrong
			return 1
		}
		if result := compare(list1.entries[i], list2.entries[i]); result != 0 {
			// deviating entry found with which we can decide the order
			return result
		}
	}
	if len(list1.entries) < len(list2.entries) {
		return -1
	}
	return 0
}

func compareLiterals(lit1, lit2 *Literal) int {
	if lit1.value < lit2.value {
		return -1
	} else if lit1.value > lit2.value {
		return 1
	} else {
		return 0
	}
}

func parse(raw []byte) interface{} {
	stack := []interface{}{&List{}}
	var text string
	for i := 0; i < len(raw); i++ {
		list := stack[len(stack)-1].(*List)
		if digit.IsDigit(raw[i]) {
			text += string(raw[i])
		} else if raw[i] == LIST_SEPARATOR {
			if len(text) > 0 {
				// entry was a literal, so there is "left-over" text to convert.
				text = parseText(list, text)
			}
			// next list entry coming up, so no further action
		} else if raw[i] == '[' {
			assert.Require(len(text) == 0, "Expected text to already be converted")
			nested := List{}
			list.entries = append(list.entries, &nested)
			stack = append(stack, &nested)
		} else if raw[i] == ']' {
			if len(text) > 0 {
				text = parseText(list, text)
			}
			stack = stack[:len(stack)-1]
		} else {
			panic("unknown symbol encountered")
		}
	}
	assert.Equal(1, len(stack))
	return stack[0].(*List).entries[0]
}

func parseText(list *List, text string) string {
	assert.Require(len(text) > 0, "Expected actual content for conversion")
	list.entries = append(list.entries, Literal{strconv.MustParseUint[uint](text, 10)})
	return ""
}

func print(value interface{}) []byte {
	switch v := value.(type) {
	case *List:
		contents := bytes.Join(slices.Transform(v.entries, print),
			[]byte{LIST_SEPARATOR})
		return append([]byte{'['}, append(contents, ']')...)
	case Literal:
		return []byte(fmt.Sprintf("%v", v.value))
	default:
		panic("unknown type")
	}
}

type List struct {
	entries []interface{}
}

type Literal struct {
	value uint
}

const LIST_SEPARATOR = ','
