package main

import (
	"bytes"
	"fmt"

	"github.com/cobratbq/goutils/assert"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	io_ "github.com/cobratbq/goutils/std/io"
)

func main() {
	reader, closer, openErr := bufio_.OpenFileReadOnly("input.txt")
	assert.Success(openErr, "Failed to load input from file")
	defer io_.ClosePanicked(closer, "Failed to close input file")
	var total uint64 = 0
	assert.Success(bufio_.ReadBytesLinesNoDelimFunc(reader, '\n', func(input []byte) error {
		if len(input) == 3 {
			challenge, outcome, ok := bytes.Cut(input, []byte{' '})
			assert.Require(ok, "Illegally formatted line encountered.")
			score := anticipate(challenge[0], outcome[0])
			total += uint64(score)
			fmt.Println(score, "++", total)
		}
		return nil
	}), "Unexpected error reading line from file")
	fmt.Println("Total:", total)
}

func anticipate(chal, result byte) uint8 {
	var c uint8
	switch chal {
	case 'A':
		c = ROCK
	case 'B':
		c = PAPER
	case 'C':
		c = SCISSORS
	default:
		panic("Unsupported use case for challenge")
	}
	switch result {
	case 'X':
		r := (c + 2) % 3
		return 0 + r + 1
	case 'Y':
		return 3 + c + 1
	case 'Z':
		r := (c + 1) % 3
		return 6 + r + 1
	default:
		panic("Illegal case encountered")
	}
}

func evaluate(chal, resp byte) uint8 {
	var c, r uint8
	switch chal {
	case 'A':
		c = ROCK
	case 'B':
		c = PAPER
	case 'C':
		c = SCISSORS
	default:
		panic("Unsupported use case for challenge")
	}
	switch resp {
	case 'X':
		r = ROCK
	case 'Y':
		r = PAPER
	case 'Z':
		r = SCISSORS
	default:
		panic("Unsupported use case for response")
	}
	switch int8(r) - int8(c) {
	case 0:
		return 3 + r + 1
	case -2, 1:
		return 6 + r + 1
	case -1, 2:
		return 0 + r + 1
	default:
		panic("Illegal case encountered.")
	}
}

const (
	ROCK uint8 = iota
	PAPER
	SCISSORS
)
