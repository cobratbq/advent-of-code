package main

import (
	"bytes"
	"fmt"

	"github.com/cobratbq/goutils/assert"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	io_ "github.com/cobratbq/goutils/std/io"
	"github.com/cobratbq/goutils/std/strconv"
)

func main() {
	reader, closer, inputErr := bufio_.OpenFileReadOnly("input.txt")
	assert.Success(inputErr, "Failed to open input file")
	defer io_.ClosePanicked(closer, "Failed to gracefully close input file")
	count := 0
	assert.Success(bufio_.ReadBytesLinesNoDelimFunc(reader, '\n', func(line []byte) error {
		if len(line) == 0 {
			return nil
		}
		first, second, found := bytes.Cut(line, []byte{','})
		assert.Require(found, "Illegal input line")
		firstLow, firstHigh := parseRange(first)
		secondLow, secondHigh := parseRange(second)
		if firstHigh < secondLow || secondHigh < firstLow {
			// no overlap
		} else {
			count++
		}
		return nil
	}), "Failed to read line from input file")
	fmt.Println("Total:", count)
}

func parseRange(rang []byte) (uint8, uint8) {
	low, high, found := bytes.Cut(rang, []byte{'-'})
	assert.Require(found, "Illegal range definition")
	return strconv.MustParseUint[uint8](string(low), 10), strconv.MustParseUint[uint8](string(high), 10)
}
