package main

import (
	"os"
	"strconv"
	"strings"

	"github.com/cobratbq/goutils/assert"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	"github.com/cobratbq/goutils/std/builtin"
	"github.com/cobratbq/goutils/std/builtin/maps"
	"github.com/cobratbq/goutils/std/builtin/set"
	"github.com/cobratbq/goutils/std/errors"
	io_ "github.com/cobratbq/goutils/std/io"
	"github.com/cobratbq/goutils/std/log"
	"github.com/cobratbq/goutils/types"
)

var Source = coord{x: 500, y: 0}

const Separator = " -> "

func main() {
	cave := make(map[coord]struct{})
	readInput("input.txt", cave)
	sand := make(map[coord]struct{})
	height := bounds(cave) + 2
	simulate(Source, cave, sand, height)
}

func simulate(source coord, cave, sand map[coord]struct{}, height uint) {
	var next coord
	for {
		previousCount := len(sand)
		grain := source
		for !set.Contains(sand, source) {
			if next.x, next.y = grain.x, grain.y+1; qualify(cave, sand, height, next) {
				grain = next
				continue
			}
			if next.x, next.y = grain.x-1, grain.y+1; qualify(cave, sand, height, next) {
				grain = next
				continue
			}
			if next.x, next.y = grain.x+1, grain.y+1; qualify(cave, sand, height, next) {
				grain = next
				continue
			}
			log.Debugln("Grain settled at", grain)
			set.Insert(sand, grain)
			break
		}
		if len(sand) == previousCount {
			break
		}
	}
	log.Infoln("Simulation finished at", len(sand))
}

func qualify(cave, sand map[coord]struct{}, height uint, coord coord) bool {
	if _, caveUsed := cave[coord]; caveUsed {
		return false
	}
	if _, sandUsed := sand[coord]; sandUsed {
		return false
	}
	if coord.y >= height {
		return false
	}
	return true
}

func bounds(cave map[coord]struct{}) uint {
	return maps.FoldKeys(cave, 0, func(r uint, k coord) uint {
		if r < k.y {
			return k.y
		} else {
			return r
		}
	})
}

func readInput(name string, cave map[coord]struct{}) {
	reader, closer, inputErr := bufio_.OpenFileReadOnly(name)
	assert.Success(inputErr, "Failed to open input file")
	defer io_.ClosePanicked(closer, "Failed to close input file")
	_ = bufio_.ReadStringLinesFunc(reader, '\n', func(line string) error {
		if len(line) == 0 {
			return nil
		}
		fromText, toText, found := strings.Cut(line, Separator)
		assert.Require(found, "Incomplete line encountered")
		var remains string
		for len(toText) > 0 {
			toText, remains, _ = strings.Cut(toText, Separator)
			plotRange(cave,
				builtin.Expect(parseCoord(fromText)),
				builtin.Expect(parseCoord(toText)))
			fromText, toText = toText, remains
		}
		return nil
	})
	log.Debugf("%+v", cave)
}

func plotRange(cave map[coord]struct{}, from, to coord) {
	if from.x == to.x {
		// range over y
		for i := from.y; i <= to.y; i++ {
			set.Insert(cave, coord{x: from.x, y: i})
		}
		for i := from.y; i >= to.y; i-- {
			set.Insert(cave, coord{x: from.x, y: i})
		}
	} else if from.y == to.y {
		// range over x
		for i := from.x; i <= to.x; i++ {
			set.Insert(cave, coord{x: i, y: from.y})
		}
		for i := from.x; i >= to.x; i-- {
			set.Insert(cave, coord{x: i, y: from.y})
		}
	} else {
		panic("Unsupported case with both x and y change value")
	}
}

func parseCoord(text string) (coord, error) {
	xtext, ytext, ok := strings.Cut(text, ",")
	if !ok {
		return coord{}, os.ErrInvalid
	}
	var err error
	var x, y uint64
	x, err = strconv.ParseUint(xtext, 10, types.UintSize)
	if err != nil {
		return coord{}, errors.Context(err, "failed to parse x-coordinate")
	}
	y, err = strconv.ParseUint(ytext, 10, types.UintSize)
	if err != nil {
		return coord{}, errors.Context(err, "failed to parse y-coordinate")
	}
	return coord{x: uint(x), y: uint(y)}, nil
}

type coord struct {
	x uint
	y uint
}
