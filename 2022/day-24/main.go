package main

import (
	"github.com/cobratbq/goutils/assert"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	"github.com/cobratbq/goutils/std/builtin"
	"github.com/cobratbq/goutils/std/builtin/set"
	"github.com/cobratbq/goutils/std/builtin/slices"
	"github.com/cobratbq/goutils/std/errors"
	io_ "github.com/cobratbq/goutils/std/io"
	"github.com/cobratbq/goutils/std/log"
	"github.com/cobratbq/goutils/std/math"
	"github.com/cobratbq/goutils/std/math/modular"
)

func main() {
	sim := builtin.Expect(readInput("input.txt"))
	path, err := simulate(&sim, sim.start, sim.exit, sim.start, sim.exit)
	assert.Success(err, "Failed to find path to exit")
	log.Infoln("Start:", sim.start, "Exit:", sim.exit)
	log.Infoln("Solution:", len(path), path)
}

func simulate(sim *simulation, start pos, dests ...pos) ([]pos, error) {
	current := opt{time: 0, position: start}
	for _, target := range dests {
		stack := []opt{current}
		for len(stack) > 0 {
			current = stack[0]
			//log.Debugln("Currently:", current.time)
			if current.position == target {
				break
			}
			for _, move := range sim.next(current.time, current.position) {
				step := opt{time: current.time + 1, position: move}
				if !slices.Contains(stack[1:], step) {
					stack = append(stack, step)
				}
			}
			stack = stack[1:]
		}
		log.Infoln("Reached a target:", target, current)
	}
	// FIXME this will always result in failure, even if destinations were reached. Not fixing right now as puzzle is completed.
	return nil, errDeadEnd
}

var errDeadEnd = errors.NewUintError(1)

type opt struct {
	time     int
	position pos
}

func readInput(name string) (simulation, error) {
	reader, closer, inputErr := bufio_.OpenFileReadOnly(name)
	assert.Success(inputErr, "Failed to open input file")
	defer io_.ClosePanicked(closer, "Failed to close input file")
	rows, readErr := bufio_.ReadProcessBytesLinesFunc(reader, '\n',
		func(line []byte) ([]byte, error) {
			if len(line) == 0 {
				return []byte{}, bufio_.ErrProcessingIgnore
			}
			return line, nil
		})
	assert.Success(readErr, "Failure while reading input from file")
	return construct(rows), nil
}

type simulation struct {
	start     pos
	exit      pos
	cyclesize int
	options   map[pos][]int
}

func (s *simulation) next(time int, current pos) []pos {
	var opts []pos
	step := (time + 1) % s.cyclesize
	for _, move := range []pos{
		{current.x + 1, current.y},
		{current.x, current.y + 1},
		current,
		{current.x - 1, current.y},
		{current.x, current.y - 1},
	} {
		if slices.Contains(s.options[move], step) {
			opts = append(opts, move)
		}
	}
	return opts
}

func construct(rows [][]byte) simulation {
	var sim simulation
	var lengths []uint
	var blizzards []blizzard
	for y := 0; y < len(rows); y++ {
		for x := 0; x < len(rows[y]); x++ {
			t := rows[y][x]
			if t == '.' {
				if y == 0 {
					sim.start = pos{x, y}
				} else if y == len(rows)-1 {
					sim.exit = pos{x, y}
				}
				// empty space, nothing to process
				continue
			}
			if t == '<' || t == '^' || t == '>' || t == 'v' {
				blizzard := extrapolateBlizzard(rows, pos{x, y})
				l := uint(len(blizzard.path))
				if !slices.Contains(lengths, l) {
					lengths = append(lengths, l)
				}
				blizzards = append(blizzards, blizzard)
			}
		}
	}
	sim.cyclesize = int(slices.Fold(lengths[1:], lengths[0], math.LCM[uint]))
	sim.options = make(map[pos][]int)
	for i := 0; i < sim.cyclesize; i++ {
		dangers := make(map[pos]struct{})
		for _, b := range blizzards {
			set.Insert(dangers, b.position(i))
		}
		log.Debugln("    iteration", i, ":", len(dangers), "dangers")
		for y := 0; y < len(rows); y++ {
			for x := 0; x < len(rows[y]); x++ {
				if rows[y][x] == '#' {
					continue
				}
				p := pos{x, y}
				if _, dangerous := dangers[p]; !dangerous {
					sim.options[p] = append(sim.options[p], i)
				}
			}
		}
	}
	log.Debugln("Options for time-index:", sim.options)
	return sim
}

func extrapolateBlizzard(rows [][]byte, start pos) blizzard {
	symbol := rows[start.y][start.x]
	assert.Any(symbol, '<', '^', '>', 'v')
	b := blizzard{symbol: symbol}
	next := start
	for {
		b.path = append(b.path, next)
		for {
			switch symbol {
			case '>':
				next = pos{modular.Increment(next.x, len(rows[next.y])), next.y}
			case 'v':
				next = pos{next.x, modular.Increment(next.y, len(rows))}
			case '<':
				next = pos{modular.Decrement(next.x, len(rows[next.y])), next.y}
			case '^':
				next = pos{next.x, modular.Decrement(next.y, len(rows))}
			default:
				panic("BUG: illegal symbol")
			}
			if rows[next.y][next.x] == '#' {
				continue
			}
			break
		}
		assert.Require(next.x >= 0, "bounds-check next.x failed")
		assert.Require(next.y >= 0, "bounds-check next.y failed")
		if b.path[0] == next {
			break
		}
	}
	return b
}

type blizzard struct {
	symbol byte
	path   []pos
}

func (b *blizzard) position(iteration int) pos {
	return b.path[modular.Reduce(iteration, len(b.path))]
}

type pos struct{ x, y int }
