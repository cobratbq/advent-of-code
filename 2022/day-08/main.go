package main

import (
	"bufio"
	"io"

	"github.com/cobratbq/goutils/assert"
	"github.com/cobratbq/goutils/codec/bytes/digit"
	"github.com/cobratbq/goutils/codec/uint/coord"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	"github.com/cobratbq/goutils/std/builtin/set"
	"github.com/cobratbq/goutils/std/errors"
	io_ "github.com/cobratbq/goutils/std/io"
	"github.com/cobratbq/goutils/std/log"
)

func main() {
	reader, closer, inputErr := bufio_.OpenFileReadOnly("input.txt")
	assert.Success(inputErr, "Failed to open input file")
	defer io_.ClosePanicked(closer, "Failed to close input file")
	grid := readGrid(reader)
	visibles := analyzeVisibility(grid)
	log.Debugln("Visible trees:", len(visibles), visibles)
	candidates, topScore := analyzeTreehouseCandidates(grid)
	log.Debugln("Candidates for treehouse:", candidates)
	log.Debugln("Top scenic score:", topScore)
}

func analyzeVisibility(grid [][]int8) map[uint]struct{} {
	rowlength := len(grid[0])
	visible := make(map[uint]struct{})
	var prev int8
	for i := 0; i < len(grid); i++ {
		prev = -1
		for j := 0; j < len(grid[i]); j++ {
			if prev == 9 {
				break
			}
			if grid[i][j] <= prev {
				continue
			}
			set.Insert(visible, coord.Encode2DInt(rowlength, j, i))
			prev = grid[i][j]
		}
		prev = -1
		for j := len(grid[i]) - 1; j >= 0; j-- {
			if prev == 9 {
				break
			}
			if grid[i][j] <= prev {
				continue
			}
			set.Insert(visible, coord.Encode2DInt(rowlength, j, i))
			prev = grid[i][j]
		}
	}
	for j := 0; j < rowlength; j++ {
		prev = -1
		for i := 0; i < len(grid); i++ {
			if prev == 9 {
				break
			}
			if grid[i][j] <= prev {
				continue
			}
			set.Insert(visible, coord.Encode2DInt(rowlength, j, i))
			prev = grid[i][j]
		}
		prev = -1
		for i := len(grid) - 1; i >= 0; i-- {
			if prev == 9 {
				break
			}
			if grid[i][j] <= prev {
				continue
			}
			set.Insert(visible, coord.Encode2DInt(rowlength, j, i))
			prev = grid[i][j]
		}
	}
	return visible
}

func analyzeTreehouseCandidates(grid [][]int8) (map[uint]uint32, uint32) {
	rowlength := len(grid[0])
	scores := make(map[uint]uint32)
	var top uint32
	for y := 0; y < len(grid); y++ {
		for x := 0; x < len(grid[y]); x++ {
			score := scoreScenic(grid, x, y)
			scores[coord.Encode2DInt(rowlength, x, y)] = score
			if top < score {
				top = score
			}
		}
	}
	return scores, top
}

func scoreScenic(grid [][]int8, x, y int) uint32 {
	var directions [4]uint32
	treehouse := grid[y][x]
	for i := y - 1; i >= 0; i-- {
		directions[0]++
		if treehouse <= grid[i][x] {
			break
		}
	}
	for i := y + 1; i < len(grid); i++ {
		directions[1]++
		if treehouse <= grid[i][x] {
			break
		}
	}
	for j := x - 1; j >= 0; j-- {
		directions[2]++
		if treehouse <= grid[y][j] {
			break
		}
	}
	for j := x + 1; j < len(grid[y]); j++ {
		directions[3]++
		if treehouse <= grid[y][j] {
			break
		}
	}
	return directions[0] * directions[1] * directions[2] * directions[3]
}

func readGrid(reader *bufio.Reader) [][]int8 {
	var grid [][]int8
	for {
		line, readErr := bufio_.ReadBytesNoDelim(reader, '\n')
		if errors.Is(readErr, io.EOF) {
			break
		}
		assert.Success(readErr, "Failed to read input line")
		row := make([]int8, 0, len(line))
		for _, v := range line {
			row = append(row, int8(digit.EncodeDigit(v)))
		}
		grid = append(grid, row)
	}
	return grid
}
