package main

import (
	"os"
	"regexp"

	"github.com/cobratbq/goutils/assert"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	"github.com/cobratbq/goutils/std/builtin"
	"github.com/cobratbq/goutils/std/log"
	"github.com/cobratbq/goutils/std/strconv"

	"github.com/draffensperger/golp"
)

/* Initial: 1 ore-collecting robot, robot-factory, blueprints.
 * Runtime: 24 minutes
 * Result: sum over all blueprints: blueprint-ID * number-of-geodes
 */

/*
Blueprint 1:
  Each ore robot costs 4 ore.
  Each clay robot costs 2 ore.
  Each obsidian robot costs 3 ore and 14 clay.
  Each geode robot costs 2 ore and 7 obsidian.

Blueprint 2:
  Each ore robot costs 2 ore.
  Each clay robot costs 3 ore.
  Each obsidian robot costs 3 ore and 8 clay.
  Each geode robot costs 3 ore and 12 obsidian.
*/

func main() {
	blueprints := builtin.Expect(readInput("sample.txt"))
	log.Debugln("Blueprints:", len(blueprints))
	// FIXME start experimenting with blueprints to find optimal strategy for each blueprint
	resourceOre := float64(4) // starting with '4' to satisfy requirements for first ore robot
	resourceClay := float64(0)
	resourceObsidian := float64(0)
	resourceGeode := float64(0)
	robotOre := float64(1)
	robotClay := float64(0)
	robotObsidian := float64(0)
	robotGeode := float64(0)
	for i := 0; i < MaxRuntime; i++ {
		lp := golp.NewLP(0, 8)
		lp.SetInt(0, true)
		lp.SetInt(1, true)
		lp.SetInt(2, true)
		lp.SetInt(3, true)
		lp.SetInt(4, true)
		lp.SetInt(5, true)
		lp.SetInt(6, true)
		lp.SetInt(7, true)
		// 0, 1, 2, 3: ore, clay, obsidian, geode
		// - each resource must be non-negative (at all times)
		lp.AddConstraint([]float64{1, 0, 0, 0, 0, 0, 0, 0}, golp.EQ, resourceOre)
		lp.AddConstraint([]float64{0, 1, 0, 0, 0, 0, 0, 0}, golp.EQ, resourceClay)
		lp.AddConstraint([]float64{0, 0, 1, 0, 0, 0, 0, 0}, golp.EQ, resourceObsidian)
		lp.AddConstraint([]float64{0, 0, 0, 1, 0, 0, 0, 0}, golp.EQ, resourceGeode)
		lp.AddConstraint([]float64{0, 0, 0, 0, 1, 0, 0, 0}, golp.GE, robotOre)
		lp.AddConstraint([]float64{0, 0, 0, 0, 0, 1, 0, 0}, golp.GE, robotClay)
		lp.AddConstraint([]float64{0, 0, 0, 0, 0, 0, 1, 0}, golp.GE, robotObsidian)
		lp.AddConstraint([]float64{0, 0, 0, 0, 0, 0, 0, 1}, golp.GE, robotGeode)
		lp.AddConstraint([]float64{0, 0, 0, 0, 1, 1, 1, 1}, golp.LE, robotOre+robotClay+robotObsidian+robotGeode+1)
		lp.AddConstraint([]float64{1, 0, 0, 0, -4, -2, -3, -2}, golp.GE, 0)
		lp.AddConstraint([]float64{0, 1, 0, 0, 0, 0, -14, 0}, golp.GE, 0)
		lp.AddConstraint([]float64{0, 0, 1, 0, 0, 0, 0, -7}, golp.GE, 0)

		lp.SetObjFn([]float64{-1, -1, -1, 0, 0, 1, 1, 1})
		lp.SetMaximize()
		solution := lp.Solve()
		log.Debugf("Solution-type: %v", solution)
		assert.Equal(solution, golp.OPTIMAL)
		result := lp.Variables()
		log.Debugln("Variables:", lp.Variables())

		resourceOre = result[0] + robotOre
		resourceClay = result[1] + robotClay
		resourceObsidian = result[2] + robotObsidian
		resourceGeode = result[3] + robotGeode
		robotOre = result[4]
		robotClay = result[5]
		robotObsidian = result[6]
		robotGeode = result[7]
	}
}

const MaxRuntime = 24

func readInput(name string) ([]blueprint, error) {
	return bufio_.OpenFileProcessStringLinesFunc(name, '\n', func(line string) (blueprint, error) {
		if line == "" {
			return blueprint{}, bufio_.ErrProcessingIgnore
		}
		return parseBlueprint(line)
	})
}

var BlueprintDefinitionPattern = regexp.MustCompile(`Blueprint (\d+): Each ore robot costs (\d+) ore. Each clay robot costs (\d+) ore. Each obsidian robot costs (\d+) ore and (\d+) clay. Each geode robot costs (\d+) ore and (\d+) obsidian.`)

func parseBlueprint(line string) (blueprint, error) {
	matches := BlueprintDefinitionPattern.FindStringSubmatch(line)
	if matches == nil {
		return blueprint{}, os.ErrInvalid
	}
	var b blueprint
	b.id = strconv.MustParseUint[uint8](matches[1], 10)
	b.oreRobot = strconv.MustParseUint[uint8](matches[2], 10)
	b.clayRobot = strconv.MustParseUint[uint8](matches[3], 10)
	b.obsidianRobot = struct {
		ore  uint8
		clay uint8
	}{
		strconv.MustParseUint[uint8](matches[4], 10),
		strconv.MustParseUint[uint8](matches[5], 10),
	}
	b.geodeRobot = struct {
		ore      uint8
		obsidian uint8
	}{
		strconv.MustParseUint[uint8](matches[6], 10),
		strconv.MustParseUint[uint8](matches[7], 10),
	}
	return b, nil
}

type blueprint struct {
	id            uint8
	oreRobot      uint8
	clayRobot     uint8
	obsidianRobot struct {
		ore  uint8
		clay uint8
	}
	geodeRobot struct {
		ore      uint8
		obsidian uint8
	}
}
