package main

import (
	"regexp"
	"strings"

	"github.com/cobratbq/goutils/assert"
	"github.com/cobratbq/goutils/std/bufio"
	"github.com/cobratbq/goutils/std/builtin"
	"github.com/cobratbq/goutils/std/builtin/maps"
	"github.com/cobratbq/goutils/std/builtin/slices"
	"github.com/cobratbq/goutils/std/log"
	"github.com/cobratbq/goutils/std/math"
	"github.com/cobratbq/goutils/std/strconv"
)

func main() {
	valves := readInput(inputName)
	sim := simulation{current: initialValve, valves: valves, release: 0}
	sim.compact()
	sim.analyze(initialClock, initialValve)
}

const inputName = "sample.txt"
const initialClock = 26
const initialValve = "AA"

type simulation struct {
	current string
	valves  map[string]*valve
	release uint
}

func (s *simulation) analyze(remaining uint, start string) {
	seq := sequence(s.valves, remaining, start)
	log.Debugln("Sequences:", seq)
}

func sequence(valves map[string]*valve, remaining uint, start string) [][]string {
	seq := [][]string{{start}}
	current := valves[start]
	for t, c := range current.tunnels {
		if remaining <= c {
			log.Debugln("Skipping", t, remaining)
			continue
		}
		log.Debugln(start, "remaining:", remaining)
		for _, s := range sequence(valves, remaining-c, t) {
			seq = append(seq, append([]string{current.id}, s...))
		}
	}
	return seq
}

func (s *simulation) compact() {
	for _, v := range s.valves {
		v.tunnels = compactTunnelRefs(s.valves, v.id, nil)
	}
	log.Infoln("Valves (compacted):")
	for id, v := range s.valves {
		assert.Equal(id, v.id)
		log.Infof("%s: %+v", v.id, v)
	}
}

func compactTunnelRefs(valves map[string]*valve, current string, prev []string) map[string]uint {
	compacted := make(map[string]uint)
	for t, c := range valves[current].tunnels {
		if slices.Contains(prev, t) {
			// avoid cycles, this entry will already be present in the merged compacted list
			continue
		}
		if valves[t].rate > 0 {
			compacted[t] = c
			continue
		}
		subcompacted := compactTunnelRefs(valves, t, append([]string{current}, prev...))
		maps.UpdateValue(subcompacted, func(_ string, v uint) uint { return v + 1 })
		maps.MergeIntoFunc(compacted, subcompacted, math.Min[uint])
	}
	return compacted
}

func (s *simulation) potential(name string) uint {
	return (1 - s.valves[name].open) * s.valves[name].rate
}

func readInput(name string) map[string]*valve {
	return slices.ConvertToMap(builtin.Expect(bufio.OpenFileProcessStringLinesFunc(name, '\n',
		func(line string) (*valve, error) {
			if len(line) == 0 {
				return nil, bufio.ErrProcessingIgnore
			}
			v := parseValveDefinition(line)
			return &v, nil
		})),
		func(idx int, v *valve) (string, *valve) { return v.id, v })
}

func parseValveDefinition(in string) valve {
	matches := ValveDefinitionPattern.FindStringSubmatch(in)
	assert.Required(matches, "Expect all input to conform to the Valve Definition pattern.")
	var valve valve
	valve.id = matches[1]
	valve.open = 0
	valve.rate = strconv.MustParseUint[uint](matches[2], 10)
	// cost of traveling the tunnel is always 1 (non-compacted input data)
	valve.tunnels, _ = slices.ConvertToMapKeys(strings.Split(matches[3], ", "),
		func(_ int, _ string) uint { return 1 })
	// NOTE: we could verify bi-directionality of tunnels
	return valve
}

var ValveDefinitionPattern = regexp.MustCompile(`^Valve ([A-Z]{2}) has flow rate=(\d+); tunnels? leads? to valves? ([A-Z]{2}(?:, [A-Z]{2})*)$`)

type valve struct {
	id      string
	open    uint
	rate    uint
	tunnels map[string]uint
}
