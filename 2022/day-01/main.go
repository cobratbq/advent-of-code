package main

import (
	"fmt"
	"strconv"

	"github.com/cobratbq/goutils/assert"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	io_ "github.com/cobratbq/goutils/std/io"
)

func main() {
	reader, closer, err := bufio_.OpenFileReadOnly("input.txt")
	assert.Success(err, "Failed to open input file")
	defer io_.ClosePanicked(closer, "Failed to close input file")
	var (
		elves               []uint64 = make([]uint64, 0)
		topCumulativeFirst  uint64   = 0
		topCumulativeSecond uint64   = 0
		topCumulativeThird  uint64   = 0
	)
	var currentCumulative uint64 = 0
	assert.Success(bufio_.ReadStringLinesNoDelimFunc(reader, '\n',
		func(line string) error {
			if line == "" {
				// elf content separator. Save concluded totals.
				elves = append(elves, currentCumulative)
				if topCumulativeFirst < currentCumulative {
					topCumulativeThird = topCumulativeSecond
					topCumulativeSecond = topCumulativeFirst
					topCumulativeFirst = currentCumulative
				} else if topCumulativeSecond < currentCumulative {
					topCumulativeThird = topCumulativeSecond
					topCumulativeSecond = currentCumulative
				} else if topCumulativeThird < currentCumulative {
					topCumulativeThird = currentCumulative
				}
				fmt.Println("Elf concluded: ", currentCumulative)
				currentCumulative = 0
			} else {
				// continued elf content
				value, convErr := strconv.ParseUint(line, 10, 64)
				assert.Success(convErr, "Failed to convert calories into uint")
				currentCumulative += value
				fmt.Println(line)
			}
			return nil
		}), "Failed to read input without failure")
	fmt.Printf("%+v\n", elves)
	fmt.Println("Top cumulatives:", topCumulativeFirst, topCumulativeSecond, topCumulativeThird)
	fmt.Println("Sum-total:", topCumulativeFirst+topCumulativeSecond+topCumulativeThird)
}
