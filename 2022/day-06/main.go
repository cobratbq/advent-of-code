package main

import (
	"fmt"
	"io"

	"github.com/cobratbq/goutils/assert"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	"github.com/cobratbq/goutils/std/builtin"
	"github.com/cobratbq/goutils/std/builtin/multiset"
	io_ "github.com/cobratbq/goutils/std/io"
)

/*
 * mjqjpqmgbljsphdztnvjfqwrcgsmlb
 * bvwbjplbgvbhsrlpgdmjqwftvncz: first marker after character 5
 * nppdvjthqldpwncqszvftbrmjlhg: first marker after character 6
 * nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg: first marker after character 10
 * zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw: first marker after character 11
 */

const COMM_WINDOW_SIZE = 4
const MSG_WINDOW_SIZE = 14

func main() {
	reader, closer, inputErr := bufio_.OpenFileReadOnly("input.txt")
	assert.Success(inputErr, "Failed to open input file")
	defer io_.ClosePanicked(closer, "Failed to close input file")
	line, readErr := bufio_.ReadBytesNoDelim(reader, '\n')
	assert.Success(readErr, "Failed to read communication input bytes")
	fmt.Println("Start-marker finished at:", builtin.Expect(indexStartOfComm(line)))
	fmt.Println("Message-marker finished at:", builtin.Expect(indexStartOfMsg(line)))
}

func indexStartOfMsg(input []byte) (int, error) {
	assert.Require(len(input) > MSG_WINDOW_SIZE, "Expected larger input for message-marker")
	// The naive solution fills a map on every movement of the window. The first time the map
	// contains items equal to the window-size, we know we have found all-distinct characters.
	// However this produces a lot of short-lived memory for each map-fill-and-throw-away. This
	// approach controls memory-use by counting as it iterates over the input.
	window := make(map[byte]uint)
	for i := 0; i < len(input); i++ {
		assert.Require(len(window) <= MSG_WINDOW_SIZE, "Invariant for maximum window size violated.")
		if i >= MSG_WINDOW_SIZE {
			multiset.Remove(window, input[i-MSG_WINDOW_SIZE])
		}
		multiset.Insert(window, input[i])
		if len(window) == MSG_WINDOW_SIZE {
			// current item marks the end of the message-marker
			return i + 1, nil
		}
	}
	return 0, io.ErrUnexpectedEOF
}

func indexStartOfComm(input []byte) (int, error) {
	for i := COMM_WINDOW_SIZE; i < len(input); i++ {
		window := input[i-COMM_WINDOW_SIZE : i]
		if window[0] != window[1] && window[1] != window[2] && window[0] != window[2] &&
			window[0] != window[3] && window[1] != window[3] && window[2] != window[3] {
			return i, nil
		}
	}
	return 0, io.ErrUnexpectedEOF
}
