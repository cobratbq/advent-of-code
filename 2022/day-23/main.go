package main

import (
	"os"

	"github.com/cobratbq/goutils/assert"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	"github.com/cobratbq/goutils/std/builtin"
	"github.com/cobratbq/goutils/std/builtin/maps"
	"github.com/cobratbq/goutils/std/builtin/multiset"
	"github.com/cobratbq/goutils/std/builtin/slices"
	io_ "github.com/cobratbq/goutils/std/io"
	"github.com/cobratbq/goutils/std/log"
)

func main() {
	elves := readInput("input.txt")
	os.Stdout.WriteString("Initial:\n")
	initialMinX, initialMinY, initialMaxX, initialMaxY := findExtremes(elves)
	printPositioning(elves, initialMinX, initialMinY, initialMaxX, initialMaxY)
	for i := 0; ; i++ {
		// TODO need to consider planned positions of other elves too?
		log.Infoln("Round:", i+1)
		planned := plan(i, elves)
		if len(planned) == 0 {
			log.Infoln("No elves need to move anymore. Fixed-point state achieved.")
			break
		}
		elves = execute(elves, planned)
	}
	minX, minY, maxX, maxY := findExtremes(elves)
	log.Infoln("Elves:", len(elves))
	sizeX, sizeY := maxX-minX+1, maxY-minY+1
	log.Infoln("Grid-size:", sizeX, sizeY)
	log.Infoln("Tiles:", sizeX*sizeY, "empty:", sizeX*sizeY-len(elves))
}

var center = pos{0, 0}

func printPositioning(elves map[pos]intention, minX, minY, maxX, maxY int) {
	for y := minY - 1; y < maxY+1; y++ {
		for x := minX - 1; x < maxX+1; x++ {
			p := pos{x: x, y: y}
			if _, present := elves[p]; present {
				os.Stdout.Write([]byte{'#'})
			} else if p == center {
				os.Stdout.WriteString("✖")
			} else {
				os.Stdout.Write([]byte{'.'})
			}
		}
		os.Stdout.Write([]byte{'\n'})
	}
}

func findExtremes(elves map[pos]intention) (int, int, int, int) {
	var minX, minY, maxX, maxY int
	for p := range elves {
		if p.x < minX {
			minX = p.x
		}
		if p.y < minY {
			minY = p.y
		}
		if p.x > maxX {
			maxX = p.x
		}
		if p.y > maxY {
			maxY = p.y
		}
	}
	return minX, minY, maxX, maxY
}

func execute(elves map[pos]intention, plan map[pos]uint) map[pos]intention {
	updated := make(map[pos]intention)
	for current, intent := range elves {
		if intent == stay {
			updated[current] = stay
			continue
		}
		next := nextpos(current, intent)
		count, found := plan[next]
		assert.Require(found, "Expect plan to be consistent.")
		if count == 1 {
			updated[next] = stay
		} else {
			updated[current] = stay
		}
	}
	assert.Equal(len(elves), len(updated))
	return updated
}

func plan(iter int, elves map[pos]intention) map[pos]uint {
	plan := make(map[pos]uint)
	for p := range elves {
		northClear := !maps.ContainsAny(elves,
			pos{p.x - 1, p.y - 1}, pos{p.x, p.y - 1}, pos{p.x + 1, p.y - 1})
		eastClear := !maps.ContainsAny(elves,
			pos{p.x + 1, p.y - 1}, pos{p.x + 1, p.y}, pos{p.x + 1, p.y + 1})
		southClear := !maps.ContainsAny(elves,
			pos{p.x - 1, p.y + 1}, pos{p.x, p.y + 1}, pos{p.x + 1, p.y + 1})
		westClear := !maps.ContainsAny(elves,
			pos{p.x - 1, p.y - 1}, pos{p.x - 1, p.y}, pos{p.x - 1, p.y + 1})
		if northClear && eastClear && southClear && westClear {
			elves[p] = stay
			continue
		}
		var intent intention
		for i := 0; i < 4; i++ {
			if (iter+i)%4 == 0 && northClear {
				intent = north
				break
			} else if (iter+i)%4 == 1 && southClear {
				intent = south
				break
			} else if (iter+i)%4 == 2 && westClear {
				intent = west
				break
			} else if (iter+i)%4 == 3 && eastClear {
				intent = east
				break
			}
		}
		elves[p] = intent
		multiset.Insert(plan, nextpos(p, intent))
	}
	return plan
}

func nextpos(p pos, c intention) pos {
	switch c {
	case stay:
		return p
	case north:
		return pos{x: p.x, y: p.y - 1}
	case east:
		return pos{x: p.x + 1, y: p.y}
	case south:
		return pos{x: p.x, y: p.y + 1}
	case west:
		return pos{x: p.x - 1, y: p.y}
	default:
		panic("unsupported intention")
	}
}

func readInput(name string) map[pos]intention {
	reader, closer, inputErr := bufio_.OpenFileReadOnly(name)
	assert.Success(inputErr, "Failed to open input file")
	defer io_.ClosePanicked(closer, "Failed to close input file")
	lines := builtin.Expect(bufio_.ReadProcessBytesLinesFunc(reader, '\n',
		func(line []byte) ([]int, error) {
			if len(line) == 0 {
				return nil, bufio_.ErrProcessingIgnore
			}
			middle := builtin.Expect(slices.MiddleIndex(line))
			positions := slices.FoldIndexed(line, []int{}, func(r []int, idx int, e byte) []int {
				if e == '#' {
					return append(r, idx-middle)
				}
				return r
			})
			return positions, nil
		}))
	middle := builtin.Expect(slices.MiddleIndex(lines))
	elves := make(map[pos]intention)
	for y, line := range lines {
		for _, x := range line {
			// store elves with position relative to center of the map
			elves[pos{x: x, y: y - middle}] = stay
		}
	}
	return elves
}

type pos struct{ x, y int }

const (
	stay intention = iota
	north
	east
	south
	west
)

type intention uint8
