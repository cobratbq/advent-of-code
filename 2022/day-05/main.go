package main

import (
	"regexp"

	"github.com/cobratbq/goutils/assert"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	io_ "github.com/cobratbq/goutils/std/io"
	log "github.com/cobratbq/goutils/std/log"
	"github.com/cobratbq/goutils/std/strconv"
)

var PatternMovement = regexp.MustCompile(`^move (\d+) from (\d+) to (\d+)$`)

func main() {
	reader, closer, fileErr := bufio_.OpenFileReadOnly("input.txt")
	assert.Success(fileErr, "Failed to open input file")
	defer io_.ClosePanicked(closer, "Failed to gracefully close input file")
	// The instructions describe the input as being a drawing of the initial situation. For that
	// reason, I will not try to parse the first input as-if it was intended for data consumption.
	// Instead, the initial configuration is copied into the data structure below.
	//var crates = [][]byte{
	//	{'Z', 'N'},
	//	{'M', 'C', 'D'},
	//	{'P'},
	//}
	var crates = [][]byte{
		{'R', 'N', 'F', 'V', 'L', 'J', 'S', 'M'},
		{'P', 'N', 'D', 'Z', 'F', 'J', 'W', 'H'},
		{'W', 'R', 'C', 'D', 'G'},
		{'N', 'B', 'S'},
		{'M', 'Z', 'W', 'P', 'C', 'B', 'F', 'N'},
		{'P', 'R', 'M', 'W'},
		{'R', 'T', 'N', 'G', 'L', 'S', 'W'},
		{'Q', 'T', 'H', 'F', 'N', 'B', 'V'},
		{'L', 'M', 'H', 'Z', 'N', 'F'},
	}
	assert.Success(bufio_.ReadStringLinesNoDelimFunc(reader, '\n', func(line string) error {
		if len(line) == 0 || line[0] == '#' {
			log.Debugln("Skipping line")
			return nil
		}
		log.Debugf("Processing line: %s", line)
		match := PatternMovement.FindStringSubmatch(line)
		var count = strconv.MustParseInt[int](match[1], 10)
		var sourceIdx = strconv.MustParseUint[uint16](match[2], 10) - 1
		var destIdx = strconv.MustParseUint[uint16](match[3], 10) - 1
		log.Debugln(count, sourceIdx, destIdx)
		// Part 1: requires moving crates one-by-one.
		//for i := 0; i < count; i++ {
		//	sourceStack := crates[sourceIdx]
		//	crates[sourceIdx] = sourceStack[:len(sourceStack)-1]
		//	crate := sourceStack[len(sourceStack)-1]
		//	crates[destIdx] = append(crates[destIdx], crate)
		//}
		// Part 2: allows moving substack as a whole.
		sourceStack := crates[sourceIdx]
		crates[sourceIdx] = sourceStack[:len(sourceStack)-count]
		crateStack := sourceStack[len(sourceStack)-count:]
		crates[destIdx] = append(crates[destIdx], crateStack...)
		printConfig(crates)
		return nil
	}), "Failed to read line from input")
}

func printConfig(crates [][]byte) {
	for i, stack := range crates {
		log.Infoln(i, string(stack))
	}
}
