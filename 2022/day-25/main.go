package main

import (
	"github.com/cobratbq/goutils/assert"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	"github.com/cobratbq/goutils/std/builtin"
	"github.com/cobratbq/goutils/std/builtin/slices"
	io_ "github.com/cobratbq/goutils/std/io"
	"github.com/cobratbq/goutils/std/log"
	"github.com/cobratbq/goutils/std/math"
)

func main() {
	values := builtin.Expect(readInput("input.txt"))
	sum := slices.Fold(values, 0, builtin.Add[uint])
	log.Infoln(sum, values)
	log.Infoln("SNAFU:", string(convertToSNAFU(sum)))
	//log.Debugln(string(convertToSNAFU(314159265)))
}

func readInput(name string) ([]uint, error) {
	reader, closer, inputErr := bufio_.OpenFileReadOnly(name)
	assert.Success(inputErr, "Failed to open input file")
	defer io_.ClosePanicked(closer, "Failed to close input file")
	return bufio_.ReadProcessBytesLinesFunc(reader, '\n', func(line []byte) (uint, error) {
		if len(line) == 0 {
			return 0, bufio_.ErrProcessingIgnore
		}
		return convertToDecimal(line), nil
	})
}

func convertToDecimal(snafu []byte) uint {
	var result uint
	snafulen := len(snafu)
	for idx, s := range snafu {
		base := math.PowUint(5, uint(snafulen-1-idx))
		switch s {
		case '=':
			result -= 2 * base
		case '-':
			result -= base
		case '0':
			// nothing
		case '1':
			result += base
		case '2':
			result += 2 * base
		}
	}
	return result
}

func convertToSNAFU(initial uint) []byte {
	result := [24]byte{'0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'}
	value := int(initial)
	var i int
	for value != 0 {
		idx := len(result) - 1 - i
		log.Debugln(value, i, string(result[:]))
		base := int(math.PowUint(5, uint(i)))
		if value > 2*base+base/2 || value < -2*base-base/2 {
			i++
			continue
		}
		if value > base+base/2 {
			result[idx] = '2'
			value -= 2 * base
		} else if value > base/2 {
			result[idx] = '1'
			value -= base
		} else if value < -base/2 && value > -base-base/2 {
			result[idx] = '-'
			value += base
		} else if value > -base-base-base/2 {
			result[idx] = '='
			value += 2 * base
		}
		i = 0
	}
	return result[:]
}
