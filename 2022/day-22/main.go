package main

import (
	"os"

	"github.com/cobratbq/goutils/assert"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	"github.com/cobratbq/goutils/std/builtin"
	"github.com/cobratbq/goutils/std/builtin/maps"
	io_ "github.com/cobratbq/goutils/std/io"
	"github.com/cobratbq/goutils/std/log"
	"github.com/cobratbq/goutils/std/strconv"
)

func main() {
	mapdata, directions := readInput("input.txt")
	current := findStartPos(mapdata)
	var orient orientation
	log.Debugln("Start:", current, "orientation:", orient)
	for _, dir := range directions {
		log.Debugln("Direction:", dir.count, string(dir.change))
		for i := uint(0); i < dir.count; i++ {
			log.Debugln("Position:", current, "orientation:", orient)
			assert.Require(maps.Contains(mapdata, current),
				"Failure: current position is not part of the map data")
			assert.Require(mapdata[current] != '#', "Failure: current position is blocked position")
			mapdata[current] = render(orient)
			var nextPos pos
			switch orient {
			case Right:
				nextPos = pos{current.x + 1, current.y}
			case Down:
				nextPos = pos{current.x, current.y + 1}
			case Left:
				nextPos = pos{current.x - 1, current.y}
			case Up:
				nextPos = pos{current.x, current.y - 1}
			default:
				panic("Invalid orientation")
			}
			nextOrient := orient
			if _, ok := mapdata[nextPos]; !ok {
				nextPos, nextOrient = wrapAround(current, orient)
			}
			if mapdata[nextPos] == '#' {
				break
			}
			current, orient = nextPos, nextOrient
		}
		orient = rotate(orient, dir.change)
	}
	printMap(mapdata)
	log.Infoln("Finished (we start from index 0):", current, string(orient))
	log.Infoln("Password:", 1000*(current.y+1)+4*(current.x+1)+int(orient))
}

func printMap(mapdata map[pos]byte) {
	for y := 0; y < 200; y++ {
		for x := 0; x < 200; x++ {
			if c, ok := mapdata[pos{x, y}]; ok {
				os.Stdout.Write([]byte{c})
			} else {
				os.Stdout.Write([]byte{' '})
			}
		}
		os.Stdout.Write([]byte{'\n'})
	}
}

func rotate(orient orientation, change byte) orientation {
	if change == 0 {
		return orient
	} else if change == 'R' {
		return (orient + 1) % 4
	} else if change == 'L' {
		return (orient + 3) % 4
	} else {
		panic("Illegal change in orientation")
	}
}

func render(orient orientation) byte {
	switch orient {
	case Right:
		return '>'
	case Down:
		return 'v'
	case Left:
		return '<'
	case Up:
		return '^'
	default:
		panic("illegal orientation")
	}
}

func wrapAround(current pos, orient orientation) (pos, orientation) {
	log.Debugln("Wrapping around the cube:", current, orient)
	if orient == Up && current.x >= 50 && current.x < 100 && current.y == 0 {
		// side a
		return pos{x: 0, y: current.x + 100}, Right
	}
	if orient == Up && current.x >= 100 && current.x < 150 && current.y == 0 {
		// side b
		return pos{x: current.x - 100, y: 199}, Up
	}
	if orient == Left && current.x == 50 && current.y >= 0 && current.y < 50 {
		// sides c (folding into a cube means y-position gets inverted)
		return pos{x: 0, y: 49 - (current.y % 50) + 100}, Right
	}
	if orient == Right && current.x == 149 && current.y >= 0 && current.y < 50 {
		// sides d (folding into a cube means y-position gets inverted)
		return pos{x: 99, y: 49 - (current.y % 50) + 100}, Left
	}
	if orient == Left && current.x == 50 && current.y >= 50 && current.y < 100 {
		// side e
		return pos{x: current.y - 50, y: 100}, Down
	}
	if orient == Down && current.x >= 100 && current.x < 150 && current.y == 49 {
		// side f
		return pos{x: 99, y: current.x - 50}, Left
	}
	if orient == Right && current.x == 99 && current.y >= 50 && current.y < 100 {
		// side g
		return pos{x: current.y + 50, y: 49}, Up
	}
	if orient == Right && current.x == 99 && current.y >= 100 && current.y < 150 {
		// side h (folding into a cube means y-position gets inverted)
		return pos{x: 149, y: 49 - (current.y % 50)}, Left
	}
	if orient == Down && current.x >= 50 && current.x < 100 && current.y == 149 {
		// side i
		return pos{x: 49, y: current.x + 100}, Left
	}
	if orient == Right && current.x == 49 && current.y >= 150 && current.y < 200 {
		// side j
		return pos{x: current.y - 100, y: 149}, Up
	}
	if orient == Down && current.x >= 0 && current.x < 50 && current.y == 199 {
		// side k
		return pos{x: current.x + 100, y: 0}, Down
	}
	if orient == Left && current.x == 0 && current.y >= 150 && current.y < 200 {
		// side l
		return pos{x: current.y - 100, y: 0}, Down
	}
	if orient == Left && current.x == 0 && current.y >= 100 && current.y < 150 {
		// side m (folding into a cube means y-position gets inverted)
		return pos{x: 50, y: 49 - (current.y % 50)}, Right
	}
	if orient == Up && current.x >= 0 && current.x < 50 && current.y == 100 {
		// side n
		return pos{x: 50, y: current.x + 50}, Right
	}
	panic("BUG: illegal case")
}

func findStartPos(mapdata map[pos]byte) pos {
	return maps.FoldKeys(mapdata, pos{999, 999}, func(current, mappos pos) pos {
		if mappos.y < current.y {
			return mappos
		}
		if mappos.y == current.y && mappos.x < current.x {
			return mappos
		}
		return current
	})
}

func readInput(name string) (map[pos]byte, []direction) {
	reader, closer, inputErr := bufio_.OpenFileReadOnly(name)
	assert.Success(inputErr, "Failed to open input file")
	defer io_.ClosePanicked(closer, "Failed to close input file")
	maplines := parseMap(builtin.Expect(bufio_.ReadProcessBytesLinesFunc(reader, '\n',
		func(line []byte) ([]byte, error) {
			if len(line) == 0 {
				// empty line is separator of map and directions
				return nil, bufio_.ErrProcessingCompleted
			}
			return line, nil
		})))
	directions := parseDirections(builtin.Expect(bufio_.ReadBytesNoDelim(reader, '\n')))
	return maplines, directions
}

func parseMap(lines [][]byte) map[pos]byte {
	output := make(map[pos]byte)
	for y, l := range lines {
		for x, c := range l {
			if c == ' ' {
				continue
			}
			output[pos{x, y}] = c
		}
	}
	return output
}

type pos struct{ x, y int }

func parseDirections(line []byte) []direction {
	var directions []direction
	for len(line) > 0 {
		count, n := strconv.ParseConsecutiveDigits[uint](line)
		var dir byte
		if len(line) > n {
			dir = line[n]
			n++
		}
		directions = append(directions, direction{count, dir})
		line = line[n:]
	}
	return directions
}

type direction struct {
	count  uint
	change byte
}

const (
	Right orientation = iota
	Down
	Left
	Up
)

type orientation uint8
