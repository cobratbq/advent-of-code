package main

import (
	"errors"
	"fmt"
	"io"

	"github.com/cobratbq/goutils/assert"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	io_ "github.com/cobratbq/goutils/std/io"
)

func main() {
	reader, closer, fileErr := bufio_.OpenFileReadOnly("input.txt")
	assert.Success(fileErr, "Failed to open input file")
	defer io_.ClosePanicked(closer, "Failed to close input file")
	var total uint64 = 0
inputloop:
	for {
		groupContents := make(map[byte]uint8)
		for j := 0; j < 3; j++ {
			mask := uint8(1 << j)
			line, readErr := bufio_.ReadBytesNoDelim(reader, '\n')
			if len(line) > 0 {
				fmt.Println("Line:", string(line))
				assert.Require(len(line)%2 == 0, "Invalid input line")
				for i := 0; i < len(line); i++ {
					item := line[i]
					groupContents[item] = groupContents[item] | mask
				}
			}
			if errors.Is(readErr, io.EOF) {
				assert.Require(j == 0, "unexpected EOF: group was partially evaluated")
				break inputloop
			}
			assert.Success(readErr, "Failed to read line from input file")
		}
		for k, v := range groupContents {
			if v == 7 {
				score := prioritize(k)
				fmt.Println(string(k), score)
				total += uint64(score)
			}
		}
	}
	fmt.Println("Total:", total)
}

func prioritize(item byte) uint8 {
	if item >= 'a' && item <= 'z' {
		return item - 'a' + 1
	}
	if item >= 'A' && item <= 'Z' {
		return item - 'A' + 27
	}
	panic("Illegal item")
}
