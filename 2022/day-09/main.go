package main

import (
	"strings"

	"github.com/cobratbq/goutils/assert"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	"github.com/cobratbq/goutils/std/builtin/multiset"
	io_ "github.com/cobratbq/goutils/std/io"
	"github.com/cobratbq/goutils/std/log"
	"github.com/cobratbq/goutils/std/strconv"
)

func main() {
	instructions := readInstructions("sample.txt")
	config := configuration{tailPos: make(map[coord]uint, 0)}
	for _, ins := range instructions {
		config.Update(ins)
	}
	log.Debugln(len(config.tailPos))
}

const NUM_KNOTS_TOTAL = 10

type configuration struct {
	head    coord
	part    [NUM_KNOTS_TOTAL - 1]coord
	tailPos map[coord]uint
}

func (c *configuration) Update(in instruction) {
	for i := uint(0); i < in.count; i++ {
		switch in.direction {
		case 'U':
			c.head.Y++
		case 'D':
			c.head.Y--
		case 'R':
			c.head.X++
		case 'L':
			c.head.X--
		default:
			panic("Illegal direction input")
		}
		c.dragTails(&c.head)
		multiset.Insert(c.tailPos, c.part[len(c.part)-1])
	}
}

func (c *configuration) dragTails(lead *coord) {
	for i := 0; i < len(c.part); i++ {
		log.Debugln("Tail", i, lead, c.part[i])
		if !c.drag(lead, &c.part[i]) {
			break
		}
		lead = &c.part[i]
	}
}

// drag simulates the dragging of following knots along the path as the head changes position. The
// return value indicates actual position updates due to dragging. If true, the dragged knot changed
// location. If false, the knot did not move.
func (c *configuration) drag(lead, tail *coord) bool {
	deltaX := lead.X - tail.X
	assert.Require(deltaX >= -2 || deltaX <= 2, "DeltaX is too large to be correctable. This should not happen.")
	deltaY := lead.Y - tail.Y
	assert.Require(deltaY >= -2 || deltaY <= 2, "DeltaY is too large to be correctable. This should not happen.")
	if deltaX <= 1 && deltaX >= -1 && deltaY <= 1 && deltaY >= -1 {
		log.Debugln("Tail is still connecting. No dragging necessary.", lead, tail)
		return false
	}
	if deltaX < 0 {
		tail.X--
	} else if deltaX > 0 {
		tail.X++
	}
	if deltaY < 0 {
		tail.Y--
	} else if deltaY > 0 {
		tail.Y++
	}
	return true
}

type coord struct {
	X int
	Y int
}

func readInstructions(file string) []instruction {
	reader, closer, inputErr := bufio_.OpenFileReadOnly(file)
	assert.Success(inputErr, "Failed to open input file")
	defer io_.ClosePanicked(closer, "Failed to close input file")
	instructions := make([]instruction, 0)
	assert.Success(bufio_.ReadStringLinesNoDelimFunc(reader, '\n',
		func(line string) error {
			if line == "" {
				return nil
			}
			direction, count, ok := strings.Cut(line, " ")
			assert.Require(ok, "Incorrect instruction input line")
			assert.Equal(1, len(direction))
			instructions = append(instructions, instruction{
				direction: direction[0],
				count:     strconv.MustParseUint[uint](count, 10),
			})
			return nil
		}),
		"Expected no failure while reading and processing file content")
	return instructions
}

type instruction struct {
	direction byte
	count     uint
}
