package main

import (
	"fmt"
	"os"
	"strings"

	"github.com/cobratbq/goutils/assert"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	"github.com/cobratbq/goutils/std/builtin"
	"github.com/cobratbq/goutils/std/builtin/slices"
	"github.com/cobratbq/goutils/std/errors"
	io_ "github.com/cobratbq/goutils/std/io"
	"github.com/cobratbq/goutils/std/log"
	"github.com/cobratbq/goutils/std/strconv"
)

func main() {
	defs := slices.ConvertToMap(builtin.Expect(readInput("input.txt")),
		func(i int, def definition) (string, interface{}) {
			return def.name, def.definition
		})
	// PART 1: evaluate expression and print result.
	rootExpr := compose(defs, "root")
	log.Infoln(calculate(rootExpr))
	// PART 2: set 'unknown' marker for human input, then reduce, then reverse.
	defs["humn"] = unknown{}
	updatedExpr := compose(defs, "root")
	rootOp := updatedExpr.(binaryOp)
	reduced1 := reduce(rootOp.comp1)
	reduced2 := reduce(rootOp.comp2)
	//log.Infoln("Reduce:", render(reduced1), render(reduced2))
	value := assert.Type[literal](reduced2, "Expected side to reduce to a literal.")
	log.Infoln("Reverse:", builtin.Expect(reverse(reduced1, value)))
}

func reverse(reduced interface{}, result literal) (literal, error) {
	switch e := reduced.(type) {
	case unknown:
		return result, nil
	case literal:
		return 0, errDeadEnd
	case binaryOp:
		var lit literal
		var nestedExp interface{}
		var side bool
		if l1, ok := e.comp1.(literal); ok {
			lit, nestedExp, side = l1, e.comp2, false
		} else if l2, ok := e.comp2.(literal); ok {
			lit, nestedExp, side = l2, e.comp1, true
		} else {
			panic("Expected at least one branch to be unresolved.")
		}
		return reverse(nestedExp, reverseOperation(e.op, side, lit, result))
	default:
		panic("Unknown type")
	}
}

func reverseOperation(op operation, side bool, val, result literal) literal {
	switch op {
	case addition:
		return result - val
	case subtraction:
		if side {
			return result + val
		} else {
			return val - result
		}
	case multiplication:
		return result / val
	case division:
		if side {
			return result * val
		} else {
			return val / result
		}
	default:
		panic("Unsupported operation")
	}
}

var errDeadEnd = errors.NewStringError("dead end: ends in literal value")

func reduce(expr interface{}) interface{} {
	switch e := expr.(type) {
	case literal:
		return e
	case unknown:
		return e
	case binaryOp:
		r1 := reduce(e.comp1)
		l1, isLiteral1 := r1.(literal)
		r2 := reduce(e.comp2)
		l2, isLiteral2 := r2.(literal)
		if isLiteral1 && isLiteral2 {
			return reduceBinaryOp(e.op, l1, l2)
		}
		return binaryOp{op: e.op, comp1: r1, comp2: r2}
	default:
		panic("BUG: illegal state")
	}
}

func reduceBinaryOp(op operation, c1, c2 literal) literal {
	switch op {
	case addition:
		return c1 + c2
	case subtraction:
		return c1 - c2
	case multiplication:
		return c1 * c2
	case division:
		return c1 / c2
	default:
		panic("BUG: unknown operation")
	}
}

func calculate(expr interface{}) literal {
	switch t := expr.(type) {
	case unknown:
		panic("Cannot handle <unknown> type")
	case literal:
		return t
	case binaryOp:
		e1 := calculate(t.comp1)
		e2 := calculate(t.comp2)
		switch t.op {
		case addition:
			return e1 + e2
		case subtraction:
			return e1 - e2
		case multiplication:
			return e1 * e2
		case division:
			return e1 / e2
		default:
			panic("BUG: unknown operation")
		}
	default:
		panic("BUG: unknown type")
	}
}

func render(expr interface{}) string {
	switch t := expr.(type) {
	case unknown:
		return "???"
	case literal:
		return fmt.Sprintf("%v", t)
	case binaryOp:
		r1 := render(t.comp1)
		r2 := render(t.comp2)
		switch t.op {
		case addition:
			return "(" + r1 + " + " + r2 + ")"
		case subtraction:
			return "(" + r1 + " - " + r2 + ")"
		case multiplication:
			return "(" + r1 + " * " + r2 + ")"
		case division:
			return "(" + r1 + " / " + r2 + ")"
		default:
			panic("BUG: unknown operation")
		}
	default:
		panic("BUG: unknown type")
	}
}

func compose(defs map[string]interface{}, name string) interface{} {
	switch e := defs[name].(type) {
	case literal, unknown:
		return e
	case binaryDesc:
		c1 := compose(defs, e.desc1)
		c2 := compose(defs, e.desc2)
		return binaryOp{op: e.op, comp1: c1, comp2: c2}
	default:
		panic("BUG: unknown type" + fmt.Sprintf("%#v", e))
	}
}

func readInput(name string) ([]definition, error) {
	reader, closer, inputErr := bufio_.OpenFileReadOnly(name)
	assert.Success(inputErr, "Failed to open input file")
	defer io_.ClosePanicked(closer, "Failed to close input file")
	return bufio_.ReadProcessStringLinesFunc(reader, '\n', func(line string) (definition, error) {
		if line == "" {
			return definition{}, bufio_.ErrProcessingIgnore
		}
		name, def, match := strings.Cut(line, ": ")
		if !match {
			return definition{}, os.ErrInvalid
		}
		return definition{name, parseMath(def)}, nil
	})
}

type definition struct {
	name       string
	definition interface{}
}

func parseMath(text string) interface{} {
	if c1, c2, match := strings.Cut(text, " + "); match {
		return binaryDesc{addition, c1, c2}
	} else if c1, c2, match := strings.Cut(text, " - "); match {
		return binaryDesc{subtraction, c1, c2}
	} else if c1, c2, match := strings.Cut(text, " * "); match {
		return binaryDesc{multiplication, c1, c2}
	} else if c1, c2, match := strings.Cut(text, " / "); match {
		return binaryDesc{division, c1, c2}
	} else {
		return strconv.MustParseUint[literal](text, 10)
	}
}

type binaryOp struct {
	op    operation
	comp1 interface{}
	comp2 interface{}
}

type binaryDesc struct {
	op    operation
	desc1 string
	desc2 string
}

type literal uint

const (
	_ operation = iota
	addition
	subtraction
	multiplication
	division
)

type operation uint8

type unknown struct{}
