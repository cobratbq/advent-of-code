package main

import (
	"fmt"
	"io"
	"path"
	"regexp"

	"github.com/cobratbq/goutils/assert"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	"github.com/cobratbq/goutils/std/errors"
	io_ "github.com/cobratbq/goutils/std/io"
	"github.com/cobratbq/goutils/std/log"
	"github.com/cobratbq/goutils/std/strconv"
)

var PatternCommandCD = regexp.MustCompile(`^\$ cd (.*)$`)
var PatternCommandLS = regexp.MustCompile(`^\$ ls$`)
var PatternOutputDir = regexp.MustCompile(`^dir (.*)$`)
var PatternOutputFile = regexp.MustCompile(`^(\d+) (.*)$`)

func main() {
	reader, closer, inputErr := bufio_.OpenFileReadOnly("input.txt")
	assert.Success(inputErr, "Failed to open input file")
	defer io_.ClosePanicked(closer, "Failed to close input file")
	var fs []Entry = []Entry{{Parent: -1, Name: "/", Size: 0}}
	var cur int = -1
	var listing bool
	line, readErr := bufio_.ReadStringNoDelim(reader, '\n')
	for !errors.Is(readErr, io.EOF) {
		assert.Success(readErr, "Failed to read input line from file")
		if matches := PatternCommandCD.FindStringSubmatch(line); matches != nil {
			listing = false
			dest := matches[1]
			if dest == "/" {
				cur = 0
				log.Debugln("Entering root directory")
			} else if dest == ".." {
				cur = fs[cur].Parent
				log.Debugln("Entering parent directory", cur, fs[cur].Name)
			} else {
				log.Debugln("Current:", cur, fs[cur].Name, ", searching for", dest)
				var i int
				for i = cur + 1; i < len(fs); i++ {
					entry := fs[i]
					log.Debugln(entry)
					if entry.Parent == cur && entry.Name == dest {
						cur = i
						log.Debugln("Entering", cur, dest)
						break
					}
					if entry.Parent < fs[cur].Parent {
						// failure condition: if everything of current directory's parent is
						// processed, then we must also have processed everything of current
						// directory.
						panic("No directory exists with specified name")
					}
				}
				assert.Require(i < len(fs), "Directory to enter cannot be found")
			}
		} else if matches := PatternCommandLS.FindStringSubmatch(line); matches != nil {
			// for sanity-checking, mark as true (not sure why yet)
			listing = true
		} else if matches := PatternOutputDir.FindStringSubmatch(line); matches != nil {
			assert.Require(listing, "directory listing should only show up after ls command")
			assert.Require(len(matches[1]) > 0, "Illegal directory name")
			assert.Equal(0, fs[cur].Size)
			fs = append(fs, Entry{Parent: cur, Name: matches[1], Size: 0})
			log.Debugln("Dir:", matches[1])
		} else if matches := PatternOutputFile.FindStringSubmatch(line); matches != nil {
			assert.Require(listing, "file listing should only show up after ls command")
			assert.Require(len(matches[2]) > 0, "Illegal file name")
			assert.Equal(0, fs[cur].Size)
			fs = append(fs, Entry{Parent: cur, Name: matches[2],
				Size: strconv.MustParseUint[uint](matches[1], 10)})
			log.Debugln("File:", matches[2])
		} else {
			panic("Unsupported input line: `" + line + "`")
		}
		line, readErr = bufio_.ReadStringNoDelim(reader, '\n')
	}
	var du = make(map[int]uint)
	for i := len(fs) - 1; i > 0; i-- {
		entry := fs[i]
		if entry.Size == 0 {
			du[entry.Parent] += du[i]
		} else {
			du[entry.Parent] += entry.Size
		}
	}
	for i := 0; i < len(fs); i++ {
		entry := fs[i]
		if entry.Size > 0 {
			continue
		}
		fmt.Println(absolutePath(fs, entry), du[i])
	}
	var available = (70000000 - du[0])
	var needed = 30000000 - available
	fmt.Println("Disk space", "used:", du[0], "available:", available, "needed:", needed)
	var found int
	for i := 0; i < len(fs); i++ {
		if fs[i].Size > 0 || du[i] < needed {
			continue
		}
		if du[i] < du[found] {
			found = i
		}
	}
	fmt.Println("Directory:", absolutePath(fs, fs[found]), "size:", du[found])
}

func absolutePath(fs []Entry, dir Entry) string {
	assert.Equal(0, dir.Size)
	if dir.Parent == -1 {
		return dir.Name
	} else {
		parent := fs[dir.Parent]
		return path.Join(absolutePath(fs, parent), dir.Name)
	}
}

type Entry struct {
	Parent int
	Name   string
	Size   uint
}
