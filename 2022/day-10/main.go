package main

import (
	"io"
	"os"
	"strings"

	"github.com/cobratbq/goutils/assert"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	io_ "github.com/cobratbq/goutils/std/io"
	"github.com/cobratbq/goutils/std/log"
	"github.com/cobratbq/goutils/std/strconv"
)

func main() {
	reader, closer, inputErr := bufio_.OpenFileReadOnly("sample.txt")
	assert.Success(inputErr, "Failed to open input file")
	defer io_.ClosePanicked(closer, "Failed to close input file")
	m := NewMachine()
	assert.Success(bufio_.ReadStringLinesNoDelimFunc(reader, '\n', func(inst string) error {
		if len(inst) == 0 {
			return nil
		}
		op, arg, found := strings.Cut(inst, " ")
		switch op {
		case "noop":
			m.Noop()
		case "addx":
			assert.Require(found, "Value missing from input")
			m.AddX(strconv.MustParseInt[int](arg, 10))
		}
		return nil
	}), "Failed to read instruction from input")
	log.Infoln("Signal strength:", m.Signal)
}

type machine struct {
	Clock uint
	X     *int
	crt   crt

	Signal uint
}

func NewMachine() machine {
	var x int = 1
	m := machine{
		X:   &x,
		crt: crt{X: nil, out: os.Stdout},
	}
	m.crt.X = &x
	return m
}

func (m *machine) Noop() {
	log.Debugln("NOOP instruction")
	m.Cycle()
}

func (m *machine) AddX(value int) {
	log.Debugln("ADDX instruction")
	m.Cycle()
	m.Cycle()
	*m.X += value
	log.Debugln("X is now", m.X)
}

func (m *machine) Cycle() {
	m.crt.Update(m.Clock)
	m.Clock++
	if m.Clock%40 == 20 {
		log.Infoln("Clock cycle", m.Clock, "X", m.X)
		m.Signal += m.Clock * uint(*m.X)
	}
}

type crt struct {
	X   *int
	out io.Writer
}

func (c *crt) Update(cycle uint) {
	if cycle > 0 && cycle%WIDTH == 0 {
		c.out.Write([]byte{'\n'})
	}
	pos := cycle % WIDTH
	log.Debugln("Drawing pos ", pos)
	if pos == uint(*c.X)-1 || pos == uint(*c.X) || pos == uint(*c.X)+1 {
		log.Debugln("Drawing...")
		c.out.Write([]byte{'#'})
	} else {
		log.Debugln("Nope.", c.X)
		c.out.Write([]byte{'.'})
	}
}

const HEIGHT = 6
const WIDTH = 40
