package main

import (
	"bytes"
	"os"

	"github.com/cobratbq/goutils/assert"
	"github.com/cobratbq/goutils/std/builtin"
	"github.com/cobratbq/goutils/std/builtin/bitset"
	"github.com/cobratbq/goutils/std/builtin/maps"
	"github.com/cobratbq/goutils/std/builtin/multiset"
	io_ "github.com/cobratbq/goutils/std/io"
	"github.com/cobratbq/goutils/std/log"
)

const inputFileName = "input.txt"

// const maxShapes = 1000000000000
const maxShapes = 1000000000000

func main() {
	//defer builtin.Expect(pprof.StartCPUProfilingFileBacked("cpuprofile.pprof")).Close()
	input := bytes.TrimRight(builtin.Expect(readInput(inputFileName)), "\n")
	windgen := windGenerator(input)
	var wind byte
	var nextwindidx int
	shapegen := shapeGenerator()
	var nextshapeidx uint8
	cycles := make(map[imprint]uint)
	var cave cave
	var shape []uint
	var pos uint
	var s uint
	for s < maxShapes {

		if s > 1 {
			depth := cave.topdepth()
			multiset.Insert(cycles, imprint{nextshapeidx, nextwindidx, depth})
			// this if-block was used during manual analysis (i.e. added ad-hoc to determine height-progression after specific shape count)
			if s == 4995 {
				log.Debugln("Height:", cave.top)
				return
			}
			// this if-block was manually modified to display after 2, 3, 4, 5 occurrences (cycles) detected.
			if maps.FoldValues(cycles, 0, func(r uint, v uint) uint {
				if v > r {
					return v
				}
				return r
			}) > 5 {
				// this if-block was used during manual analysis (added for specific purpose)
				log.Infoln("Periodic after", s, "shapes. Height:", cave.top)
				return
			}
			log.Debugln("Top-depth:", depth, cave.top, nextshapeidx, nextwindidx)
		}

		shape, nextshapeidx = shapegen()
		pos = (cave.top + expansion) * width
		//log.Debugln("ROCK(initial):", rock)
		var settled bool
		for !settled {
			wind, nextwindidx = windgen()
			pos = shift(&cave, shape, pos, wind)
			pos, settled = fall(&cave, shape, pos)
		}
		//log.Debugln("Settled:", shape, pos)
		s++
	}
	log.Infoln("Height:", cave.top)
}

type imprint struct {
	shape    uint8
	wind     int
	topdepth [7]uint8
}

func shift(cave *cave, shape []uint, pos uint, action byte) uint {
	var mod int
	switch action {
	case '<':
		mod = -1
	case '>':
		mod = 1
	default:
		panic("Unknown action")
	}
	if cave.collides(shape, pos, mod) {
		return pos
	}
	//log.Debugln("ROCK(", string(action), "):", shape, pos)
	return uint(int(pos) + mod)
}

func fall(cave *cave, shape []uint, pos uint) (uint, bool) {
	for _, r := range shape {
		part, idx := partition(r + pos - width)
		//log.Debugln("Fall-check:", part, idx)
		if r+pos < width || bitset.Bit(cave.body[part][:], idx) {
			cave.attach(shape, pos)
			return pos, true
		}
	}
	//log.Debugln("ROCK(dropped):", shape, pos)
	return pos - width, false
}

const width = 7
const expansion = 3 // rows

type cave struct {
	body [numPartitions][partitionSize]uint
	next uint
	top  uint
}

func (c *cave) topdepth() [width]uint8 {
	var snapshot [width]uint8
	if c.top == 0 {
		return snapshot
	}
	base := (c.top - 1) * width
	for i := uint(0); i < width; i++ {
		for d := uint8(0); d <= 255; d++ {
			part, colidx := partition(base + i - (uint(d) * width))
			if colidx < width || bitset.Bit(c.body[part][:], colidx) {
				snapshot[int(i)] = d
				break
			}
			assert.Unequal(d, 255)
		}
	}
	return snapshot
}

func (c *cave) collides(shape []uint, pos uint, mod int) bool {
	for _, r := range shape {
		p := int((r+pos)%width) + mod
		if p < 0 || p >= width {
			return true
		}
		part, idx := partition(uint(int(r+pos) + mod))
		if bitset.Bit(c.body[part][:], idx) {
			return true
		}
	}
	return false
}

func (c *cave) attach(shape []uint, pos uint) {
	lastidx := shape[len(shape)-1] + pos
	//log.Debugln("Lastidx:", lastidx)
	// FIXME consider clearing part current-2 for next use.
	if lastidx >= c.top*width {
		c.top = lastidx/width + 1
	}
	if lastidx/partitionSize >= c.next {
		if c.next+1 >= numPartitions {
			nextContainer := (c.next + 1) % numPartitions
			//log.Debugln("Clearing:", nextContainer, c.next+1)
			c.body[nextContainer] = [partitionSize]uint{}
		}
		c.next++
	}
	for _, r := range shape {
		part, idx := partition(r + pos)
		//log.Debugln("Attach", part, idx)
		bitset.Insert(c.body[part][:], idx)
	}
}

func partition(idx uint) (uint, uint) {
	return idx / partitionSize % numPartitions, idx % partitionSize
}

const numPartitions = 4 // number of partitions
const partitionSize = 4 * 1024 * 1024

func shapeGenerator() func() ([]uint, uint8) {
	var i uint8
	assert.Equal(numShapes, len(shapes))
	return func() ([]uint, uint8) {
		shape := shapes[i]
		i = (i + 1) % numShapes
		return shape, i
	}
}

const numShapes = 5

var shapes = [numShapes][]uint{
	{2, 3, 4, 5},
	{3, 9, 10, 11, 17},
	{2, 3, 4, 11, 18},
	{2, 9, 16, 23},
	{2, 3, 9, 10},
}

func windGenerator(input []byte) func() (byte, int) {
	var i int
	length := len(input)
	return func() (byte, int) {
		action := input[i]
		i = (i + 1) % length
		return action, i
	}
}

func readInput(name string) ([]byte, error) {
	input, inputErr := os.Open(name)
	assert.Success(inputErr, "Failed to open input file")
	defer io_.ClosePanicked(input, "Failed to close input file")
	return io_.ReadAll(input)
}
