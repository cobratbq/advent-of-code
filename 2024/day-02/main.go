package main

import (
	"bufio"
	"bytes"
	"io"
	"os"

	"github.com/cobratbq/goutils/assert"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	"github.com/cobratbq/goutils/std/builtin/slices"
	"github.com/cobratbq/goutils/std/log"
	"github.com/cobratbq/goutils/std/math"
	"github.com/cobratbq/goutils/std/strconv"
)

func readInput(in io.Reader) ([][]uint, error) {
	reader := bufio.NewReader(in)
	return bufio_.ReadProcessBytesLinesFunc[[]uint](reader, '\n', func(line []byte) ([]uint, error) {
		if len(line) == 0 {
			return nil, bufio_.ErrProcessingIgnore
		}
		entries := bytes.Fields(line)
		var values []uint
		for i := range entries {
			values = append(values, strconv.MustParseBytesUintDecimal[uint](entries[i]))
		}
		return values, nil
	})
}

func evalSafety(report []uint) (bool, int) {
	inc := report[1] > report[0]
	for i := range report {
		if i == 0 {
			continue
		}
		diff := math.Difference(report[i-1], report[i])
		if inc == (report[i] < report[i-1]) || diff == 0 || diff < 1 || diff > 3 {
			return false, i
		}
	}
	return true, 0
}

func main() {
	data, err := readInput(os.Stdin)
	assert.Success(err, "Failed to read input")
	log.Traceln("Raw data entries:", data)
	safeReports := slices.Fold[[]uint](data, 0, func(count uint, report []uint) uint {
		// evaluate both forward and backward, s.t. we can also detect very first value being an anomaly (the lazy way)
		for _, r := range [][]uint{report, slices.Reversed(report)} {
			ok, bad := evalSafety(r)
			if ok {
				return count + 1
			}
			ok, bad = evalSafety(append(r[:bad], r[bad+1:]...))
			if ok {
				return count + 1
			}
		}
		return count
	})
	log.Infoln("Safe reports:", safeReports)
}
