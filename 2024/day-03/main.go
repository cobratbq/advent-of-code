package main

import (
	"bufio"
	"io"
	"os"
	"regexp"
	"slices"

	"github.com/cobratbq/goutils/assert"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	"github.com/cobratbq/goutils/std/builtin"
	slices_ "github.com/cobratbq/goutils/std/builtin/slices"
	"github.com/cobratbq/goutils/std/log"
	"github.com/cobratbq/goutils/std/strconv"
)

var MUL_PATTERN = regexp.MustCompile(`do\(\)|don't\(\)|mul\((\d+),(\d+)\)`)

func readInput(in io.Reader) ([][2]int, error) {
	reader := bufio.NewReader(in)
	data, err := bufio_.ReadProcessStringLinesFunc[[][2]int](reader, '\n', func(line string) ([][2]int, error) {
		if len(line) == 0 {
			return nil, bufio_.ErrProcessingIgnore
		}
		// FIXME is -1 the appropriate argument for all matches?
		matches := MUL_PATTERN.FindAllStringSubmatch(line, -1)
		var muls [][2]int
		for i := range matches {
			var op [2]int
			switch matches[i][0] {
			case "do()":
				op[0] = -1
			case "don't()":
				op[0] = -2
			default:
				op[0] = strconv.MustParseIntDecimal[int](matches[i][1])
				op[1] = strconv.MustParseIntDecimal[int](matches[i][2])
			}
			muls = append(muls, op)
		}
		log.Traceln(muls)
		return muls, nil
	})
	assert.Success(err, "Failed to process input")
	return slices.Concat(data...), nil
}

func main() {
	muls := builtin.Expect(readInput(os.Stdin))
	// A little sneaky abuse of closure, so not exactly a pure construct. :-P
	var disabled bool
	muls = slices_.Filter(muls, func(op [2]int) bool {
		// Looks, let's be realistic: I went for the lazy solution here. It's practical. :-P
		if op[0] == -1 {
			disabled = false
			return false
		} else if op[0] == -2 {
			disabled = true
			return false
		}
		return !disabled
	})
	subs := slices_.Transform(muls, func(op [2]int) int { return op[0] * op[1] })
	total := slices_.Fold(subs, 0, builtin.Add)
	log.Infoln("Total:", total)
}
