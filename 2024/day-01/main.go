package main

import (
	"bufio"
	"bytes"
	"io"
	"os"
	"sort"

	"github.com/cobratbq/goutils/assert"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	"github.com/cobratbq/goutils/std/builtin/maps"
	"github.com/cobratbq/goutils/std/builtin/multiset"
	"github.com/cobratbq/goutils/std/builtin/slices"
	"github.com/cobratbq/goutils/std/log"
	"github.com/cobratbq/goutils/std/math"
	"github.com/cobratbq/goutils/std/strconv"
)

func readInput(in io.Reader) ([]int, []int, error) {
	reader := bufio.NewReader(in)
	data, err := bufio_.ReadProcessBytesLinesFunc[[]int](reader, '\n', func(line []byte) ([]int, error) {
		if len(line) == 0 {
			return nil, bufio_.ErrProcessingIgnore
		}
		data := bytes.Fields(line)
		var parts [2]int
		parts[0] = strconv.MustParseBytesInt[int](data[0], 10)
		parts[1] = strconv.MustParseBytesInt[int](data[1], 10)
		return parts[:], nil
	})
	assert.Success(err, "Failure reading data from input")
	left := slices.Transform(data, func(record []int) int { return record[0] })
	sort.Ints(left)
	right := slices.Transform(data, func(record []int) int { return record[1] })
	sort.Ints(right)
	return left, right, nil
}

func main() {
	left, right, err := readInput(os.Stdin)
	assert.Success(err, "Failed to read input")
	assert.Equal(len(left), len(right))
	log.Traceln("Left:", left)
	log.Traceln("Right:", right)
	sum := 0
	for i := range left {
		sum += math.Difference(left[i], right[i])
	}
	log.Infoln("Total difference:", sum)
	occurrences, duplicates := slices.ConvertToMapKeys(left, func(int, int) uint { return 0 })
	log.Traceln("Number of duplicates in left list:", duplicates)
	common := slices.Filter(right, func(k int) bool {
		_, ok := occurrences[k]
		return ok
	})
	multiset.InsertMany(occurrences, common)
	log.Traceln("Occurrences:", occurrences)
	similarity := maps.Fold(occurrences, 0, func(curr uint, k int, v uint) uint { return curr + uint(k)*v })
	log.Infoln("Similarity score:", similarity)
}
