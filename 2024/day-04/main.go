package main

import (
	"bufio"
	"io"
	"os"

	bufio_ "github.com/cobratbq/goutils/std/bufio"
	"github.com/cobratbq/goutils/std/builtin"
	"github.com/cobratbq/goutils/std/log"
)

func readInput(in io.Reader) ([][]byte, error) {
	reader := bufio.NewReader(in)
	return bufio_.ReadProcessBytesLinesFunc(reader, '\n', func(line []byte) ([]byte, error) {
		if len(line) == 0 {
			return nil, bufio_.ErrProcessingIgnore
		}
		return line, nil
	})
}

func take(grid [][]byte, x, y int, wordLength uint, modx, mody int) (string, bool) {
	var word = make([]byte, wordLength)
	for i := 0; i < int(wordLength); i++ {
		var nextX = x + i*modx
		var nextY = y + i*mody
		if nextX < 0 || nextY < 0 || nextX >= len(grid[y]) || nextY >= len(grid) {
			return "", false
		}
		word[i] = grid[nextY][nextX]
	}
	return string(word), true
}

const targetWord = "XMAS"

func main() {
	puzzle := builtin.Expect(readInput(os.Stdin))
	log.DebuglnSliceAsString("puzzle:", puzzle)
	var count uint
	for y := range puzzle {
		for x := range puzzle[y] {
			if puzzle[y][x] != targetWord[0] {
				continue
			}
			for _, d := range [][2]int{{1, 0}, {1, 1}, {0, 1}, {-1, 1}, {-1, 0}, {-1, -1}, {0, -1}, {1, -1}} {
				if found, ok := take(puzzle, x, y, 4, d[0], d[1]); ok && found == targetWord {
					count++
				}
			}
		}
	}
	log.Infoln("Found:", count)
}
