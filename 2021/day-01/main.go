package main

import (
	"bytes"
	"fmt"
	"io"

	"github.com/cobratbq/goutils/assert"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	"github.com/cobratbq/goutils/std/builtin"
	"github.com/cobratbq/goutils/std/errors"
	io_ "github.com/cobratbq/goutils/std/io"
	"github.com/cobratbq/goutils/std/strconv"
)

func main() {
	reader, closer, fileErr := bufio_.OpenFileReadOnly("input.txt")
	assert.Success(fileErr, "Failed to open input file.")
	defer io_.ClosePanicked(closer, "Failed to gracefully close input file")
	var increases uint = 0
	var previous [3]uint64 = [3]uint64{
		strconv.MustParseUint[uint64](string(bytes.TrimRight(builtin.Expect(reader.ReadBytes('\n')), "\n\r ")), 10),
		strconv.MustParseUint[uint64](string(bytes.TrimRight(builtin.Expect(reader.ReadBytes('\n')), "\n\r ")), 10),
		strconv.MustParseUint[uint64](string(bytes.TrimRight(builtin.Expect(reader.ReadBytes('\n')), "\n\r ")), 10),
	}
	var sum = previous[0] + previous[1] + previous[2]
	var count = 0

	var entry []byte
	var readErr error
	for readErr == nil {
		entry, readErr = reader.ReadBytes('\n')
		if errors.Is(readErr, io.EOF) {
			break
		}
		assert.Success(readErr, "Failed to read next entry")

		previous[count%3] = strconv.MustParseUint[uint64](string(bytes.TrimRight(entry, "\n\r ")), 10)
		count++

		moved := previous[0] + previous[1] + previous[2]
		if moved > sum {
			increases++
		}
		sum = moved
	}
	fmt.Println("Increases:", increases)
}
