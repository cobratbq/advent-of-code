package main

import (
	"fmt"

	"github.com/cobratbq/goutils/assert"
	"github.com/cobratbq/goutils/codec/bytes/digit"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	"github.com/cobratbq/goutils/std/builtin"
	"github.com/cobratbq/goutils/std/builtin/slices"
	"github.com/cobratbq/goutils/std/log"
	"github.com/cobratbq/goutils/types"
)

func main() {
	extended := readInput(inputFile)
	printMatrix(extended)
	cost := analyzeCost(extended)
	printMatrix(cost)
}

const inputFile = "input.txt"

func printMatrix[E types.Integer](matrix [][]E) {
	for y := 0; y < len(matrix); y++ {
		fmt.Println(matrix[y])
	}
}

func analyzeCost(matrix [][]uint8) [][]uint {
	assert.Require(slices.UniformDimensions2D(matrix), "expected uniform dimensions for matrix")
	costmtx := slices.Empty2DFrom(matrix, types.MaxUint)
	costmtx[0][0] = 0
	log.Debugln("Cost-matrix:", len(costmtx[0]), len(costmtx))
	start := next{x: len(matrix[len(matrix)-1]) - 1, y: len(matrix) - 1}
	queue := []next{start}
	costmtx[start.y][start.x] = uint(matrix[start.y][start.x])
	for i := 0; i < len(queue); i++ {
		p := queue[i]
		if p.x == 0 && p.y == 0 {
			break
		}
		incurred := costmtx[p.y][p.x]
		for _, n := range []next{{p.x - 1, p.y}, {p.x, p.y - 1}, {p.x + 1, p.y}, {p.x, p.y + 1}} {
			if n.y < 0 || n.x < 0 || n.y >= len(matrix) || n.x >= len(matrix[n.y]) ||
				costmtx[n.y][n.x] <= incurred+uint(matrix[n.y][n.x]) {
				continue
			}
			queue = append(queue, n)
			costmtx[n.y][n.x] = incurred + uint(matrix[n.y][n.x])
		}
	}
	return costmtx
}

type next struct{ x, y int }

func readInput(name string) [][]uint8 {
	matrix := builtin.Expect(bufio_.OpenFileProcessBytesLinesFunc(name, '\n', func(bytes []byte) ([]uint8, error) {
		if len(bytes) == 0 {
			return nil, bufio_.ErrProcessingIgnore
		}
		return slices.Transform(bytes, digit.DecodeDigit), nil
	}))
	extended := make([][]uint8, len(matrix)*multiplier)
	for y := 0; y < len(extended); y++ {
		matrixY := y % len(matrix)
		addY := uint8(y / len(matrix))

		extended[y] = make([]uint8, len(matrix[matrixY])*multiplier)
		for x := 0; x < len(extended[y]); x++ {
			matrixX := x % len(matrix[matrixY])
			addX := uint8(x / len(matrix[matrixY]))
			extended[y][x] = (matrix[matrixY][matrixX]-1+addY+addX)%9 + 1
		}
	}
	return extended
}

const multiplier = 5
