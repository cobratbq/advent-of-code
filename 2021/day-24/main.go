package main

import (
	"strings"

	"github.com/cobratbq/goutils/assert"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	"github.com/cobratbq/goutils/std/builtin"
	"github.com/cobratbq/goutils/std/log"
	"github.com/cobratbq/goutils/std/strconv"
)

func main() {
	program := builtin.Expect(readInput(inputFile))
	var unit alu
	unit.Parse(program)
	log.Debugln("ALU:", unit)
	result := unit.Process([]int{1, 3, 5, 7, 9, 2, 4, 6, 8, 9, 9, 9, 9, 9})
	log.Infoln("Result:", result)
}

const inputFile = "input.txt"

type alu struct {
	w, x, y, z int
	input      [14]int
	instr      []interface{ exec() }
}

func (u *alu) Process(input []int) int {
	u.w, u.x, u.y, u.z = 0, 0, 0, 0
	log.Debugln("Initial:", u.w, u.x, u.y, u.z)
	u.input = [14]int{input[0], input[1], input[2], input[3], input[4], input[5], input[6],
		input[7], input[8], input[9], input[10], input[11], input[12], input[13]}
	log.Debugln("Input:", u.input)
	for _, instr := range u.instr {
		instr.exec()
		log.Debugln("Next:", u.w, u.x, u.y, u.z)
	}
	return u.z
}

func (u *alu) Parse(lines []string) {
	var inputidx = 0
	for _, line := range lines {
		tokens := strings.Fields(line)
		log.Debugln(tokens)
		switch tokens[0] {
		case "inp":
			assert.Require(inputidx < len(u.input), "Input exceeds available input buffer.")
			u.instr = append(u.instr, &inputInstr{
				input: u.input[inputidx : inputidx+1],
				dest:  u.register(tokens[1])})
			inputidx++
		case "add":
			u.instr = append(u.instr, &addInstr{
				a: u.register(tokens[1]),
				b: u.parseValue(tokens[2])})
		case "mul":
			u.instr = append(u.instr, &mulInstr{
				a: u.register(tokens[1]),
				b: u.parseValue(tokens[2])})
		case "div":
			u.instr = append(u.instr, &divInstr{
				a: u.register(tokens[1]),
				b: u.parseValue(tokens[2])})
		case "mod":
			u.instr = append(u.instr, &modInstr{
				a: u.register(tokens[1]),
				b: u.parseValue(tokens[2])})
		case "eql":
			u.instr = append(u.instr, &eqlInstr{
				a: u.register(tokens[1]),
				b: u.parseValue(tokens[2])})
		default:
			panic("illegal instruction")
		}
	}
}

func (u *alu) parseValue(token string) *int {
	if builtin.EqualsAny(token, "w", "x", "y", "z") {
		return u.register(token)
	}
	b := strconv.MustParseInt[int](token, strconv.DecimalBase)
	return &b
}

func (u *alu) register(c string) *int {
	switch c {
	case "w":
		return &u.w
	case "x":
		return &u.x
	case "y":
		return &u.y
	case "z":
		return &u.z
	default:
		panic("illegal register")
	}
}

type inputInstr struct {
	input []int
	dest  *int
}

func (i *inputInstr) exec() {
	log.Debugln("input", i.input[0], i.input)
	*i.dest = i.input[0]
}

type addInstr struct{ a, b *int }

func (i *addInstr) exec() {
	*i.a = *i.a + *i.b
}

type mulInstr struct{ a, b *int }

func (i *mulInstr) exec() {
	*i.a = *i.a * *i.b
}

type divInstr struct{ a, b *int }

func (i *divInstr) exec() {
	*i.a = *i.a / *i.b
}

type modInstr struct{ a, b *int }

func (i *modInstr) exec() {
	*i.a = *i.a % *i.b
}

type eqlInstr struct{ a, b *int }

func (i *eqlInstr) exec() {
	if *i.a == *i.b {
		*i.a = 1
	} else {
		*i.a = 0
	}
}

func readInput(name string) ([]string, error) {
	return bufio_.OpenFileProcessStringLinesFunc(name, '\n', func(line string) (string, error) {
		if len(line) == 0 {
			return "", bufio_.ErrProcessingIgnore
		}
		return line, nil
	})
}
