package main

import (
	"github.com/cobratbq/goutils/codec/bytes/digit"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	"github.com/cobratbq/goutils/std/builtin"
	"github.com/cobratbq/goutils/std/builtin/set"
	"github.com/cobratbq/goutils/std/builtin/slices"
	"github.com/cobratbq/goutils/std/log"
	"github.com/cobratbq/goutils/std/sort"
)

func main() {
	matrix := builtin.Expect(readInput(inputFile))
	lows := analyzeLowestPoints(matrix)
	totalRiskLevel := slices.Fold(slices.Transform(lows, calcRisk(matrix)), 0, builtin.Add[uint])
	log.Infoln("Total risk level:", totalRiskLevel)
	sizes := analyzeBasins(matrix, lows)
	sort.Number(sizes)
	largestscore := slices.Fold(sizes[len(sizes)-3:], 1, builtin.Multiply[uint])
	log.Infoln("Three largest basins:", largestscore)
}

const inputFile = "input.txt"

func analyzeBasins(matrix [][]uint8, lows []pos) []uint {
	var sizes []uint
	for _, low := range lows {
		basin := make(map[pos]struct{}, 50)
		set.Insert(basin, low)
		queue := []pos{low}
		var next pos
		for i := 0; i < len(queue); i++ {
			cur := queue[i]
			for _, next = range []pos{
				{cur.x - 1, cur.y}, {cur.x + 1, cur.y},
				{cur.x, cur.y - 1}, {cur.x, cur.y + 1}} {

				if set.Contains(basin, next) ||
					next.y < 0 || next.x < 0 ||
					next.y >= len(matrix) || next.x >= len(matrix[next.y]) ||
					matrix[next.y][next.x] >= 9 {
					continue
				}
				queue = append(queue, next)
				set.Insert(basin, next)
			}
		}
		sizes = append(sizes, uint(len(basin)))
	}
	return sizes
}

func calcRisk(matrix [][]uint8) func(pos) uint {
	return func(p pos) uint {
		return uint(1 + matrix[p.y][p.x])
	}
}

func analyzeLowestPoints(matrix [][]uint8) []pos {
	var lowest []pos
	for y := 0; y < len(matrix); y++ {
		for x := 0; x < len(matrix[y]); x++ {
			if isLowest(matrix, x, y) {
				lowest = append(lowest, pos{x, y})
			}
		}
	}
	return lowest
}

func isLowest(matrix [][]uint8, x, y int) bool {
	height := matrix[y][x]
	return (x == 0 || matrix[y][x-1] > height) &&
		(y == 0 || matrix[y-1][x] > height) &&
		(y == len(matrix)-1 || matrix[y+1][x] > height) &&
		(x == len(matrix[y])-1 || matrix[y][x+1] > height)
}

type pos struct{ x, y int }

func readInput(name string) ([][]uint8, error) {
	return bufio_.OpenFileProcessBytesLinesFunc(name, '\n', func(bytes []byte) ([]uint8, error) {
		if len(bytes) == 0 {
			return nil, bufio_.ErrProcessingIgnore
		}
		return slices.Transform(bytes, digit.DecodeDigit), nil
	})
}
