package main

import (
	"github.com/cobratbq/goutils/codec/bytes/digit"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	"github.com/cobratbq/goutils/std/builtin"
	"github.com/cobratbq/goutils/std/builtin/set"
	"github.com/cobratbq/goutils/std/builtin/slices"
	"github.com/cobratbq/goutils/std/log"
)

func main() {
	matrix := builtin.Expect(readInput(inputFile))
	var flashes uint
	for i := 0; i < numSteps; i++ {
		n := process(matrix)
		flashes += n
		if n == uint(len(matrix)*len(matrix[0])) {
			log.Infoln("Synchronized:", i+1)
			break
		}
	}
	log.Infoln("Flashes:", flashes)
}

const inputFile string = "input.txt"
const numSteps = 10000
const flashThreshold = 9

func process(matrix [][]uint8) uint {
	var queue []pos
	flash := make(map[pos]struct{}, len(matrix)*len(matrix[0]))
	for y := 0; y < len(matrix); y++ {
		for x := 0; x < len(matrix[y]); x++ {
			matrix[y][x]++
			if matrix[y][x] > flashThreshold {
				p := pos{x, y}
				queue = append(queue, p)
				set.Insert(flash, p)
			}
		}
	}
	for i := 0; i < len(queue); i++ {
		doct := queue[i]
		matrix[doct.y][doct.x] = 0
		for _, d := range directions(matrix, doct.x, doct.y) {
			if set.Contains(flash, d) {
				continue
			}
			matrix[d.y][d.x]++
			if matrix[d.y][d.x] > flashThreshold {
				queue = append(queue, d)
				set.Insert(flash, d)
			}
		}
	}
	return uint(len(flash))
}

func directions(matrix [][]byte, x, y int) []pos {
	unboundLeft := x > 0
	unboundRight := x < len(matrix[0])-1
	unboundTop := y > 0
	unboundBottom := y < len(matrix)-1
	var directions []pos
	directions = slices.AppendCond(unboundTop && unboundLeft, directions, pos{x - 1, y - 1})
	directions = slices.AppendCond(unboundTop, directions, pos{x, y - 1})
	directions = slices.AppendCond(unboundTop && unboundRight, directions, pos{x + 1, y - 1})
	directions = slices.AppendCond(unboundLeft, directions, pos{x - 1, y})
	directions = slices.AppendCond(unboundRight, directions, pos{x + 1, y})
	directions = slices.AppendCond(unboundBottom && unboundLeft, directions, pos{x - 1, y + 1})
	directions = slices.AppendCond(unboundBottom, directions, pos{x, y + 1})
	directions = slices.AppendCond(unboundBottom && unboundRight, directions, pos{x + 1, y + 1})
	return directions
}

type pos struct{ x, y int }

func readInput(name string) ([][]uint8, error) {
	return bufio_.OpenFileProcessBytesLinesFunc(name, '\n', func(bytes []byte) ([]uint8, error) {
		if len(bytes) == 0 {
			return nil, bufio_.ErrProcessingIgnore
		}
		return slices.Transform(bytes, digit.DecodeDigit), nil
	})
}
