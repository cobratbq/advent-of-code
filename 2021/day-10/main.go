package main

import (
	"github.com/cobratbq/goutils/assert"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	"github.com/cobratbq/goutils/std/builtin"
	"github.com/cobratbq/goutils/std/builtin/slices"
	"github.com/cobratbq/goutils/std/log"
	"github.com/cobratbq/goutils/std/sort"
)

func main() {
	lines := builtin.Expect(readInput(inputFile))
	lines = slices.Filter(lines, func(l []byte) bool {
		return process(l) == 0
	})
	incompletes := slices.Filter(slices.Transform(lines, score), builtin.NonZero[uint])
	sort.Number(incompletes)
	log.Infoln("Middle:", incompletes[len(incompletes)/2])
}

const inputFile string = "input.txt"

func score(line []byte) uint {
	var stack []byte
	for _, b := range line {
		switch b {
		case '(', '[', '{', '<':
			stack = append(stack, b)
			continue
		case ')':
			assert.Equal('(', stack[len(stack)-1])
		case ']':
			assert.Equal('[', stack[len(stack)-1])
		case '}':
			assert.Equal('{', stack[len(stack)-1])
		case '>':
			assert.Equal('<', stack[len(stack)-1])
		default:
			panic("BUG: illegal character")
		}
		stack = stack[:len(stack)-1]
	}
	var score uint
	for len(stack) > 0 {
		score *= 5
		b := stack[len(stack)-1]
		switch b {
		case '(':
			score += 1
		case '[':
			score += 2
		case '{':
			score += 3
		case '<':
			score += 4
		default:
			panic("BUG: illegal character")
		}
		stack = stack[:len(stack)-1]
	}
	return score
}

func process(line []byte) uint {
	var stack []byte
	for _, b := range line {
		switch b {
		case '(', '[', '{', '<':
			stack = append(stack, b)
			continue
		case ')':
			if stack[len(stack)-1] != '(' {
				return 3
			}
		case ']':
			if stack[len(stack)-1] != '[' {
				return 57
			}
		case '}':
			if stack[len(stack)-1] != '{' {
				return 1197
			}
		case '>':
			if stack[len(stack)-1] != '<' {
				return 25137
			}
		default:
			panic("BUG: illegal character")
		}
		stack = stack[:len(stack)-1]
	}
	if len(stack) > 0 {
		log.Infoln("Incomplete line: stack is not empty")
	}
	return 0
}

func readInput(name string) ([][]byte, error) {
	return bufio_.OpenFileProcessBytesLinesFunc(name, '\n', func(line []byte) ([]byte, error) {
		if len(line) == 0 {
			return nil, bufio_.ErrProcessingIgnore
		}
		return line, nil
	})
}
