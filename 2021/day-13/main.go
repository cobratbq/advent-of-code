package main

import (
	"bufio"
	"bytes"
	"io"
	"os"

	"github.com/cobratbq/goutils/assert"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	"github.com/cobratbq/goutils/std/builtin/slices"
	"github.com/cobratbq/goutils/std/log"
	"github.com/cobratbq/goutils/std/math"
	"github.com/cobratbq/goutils/std/strconv"
)

func main() {
	matrix, folds := readInput(os.Stdin)
	maxY, maxX := len(matrix), len(matrix[0])
	log.Debugln("Matrix:", matrix)
	for _, f := range folds {
		assert.Unequal(0, f)
		if f > 0 {
			log.Debugln("Fold at x=", f)
			for y := 0; y < maxY; y++ {
				for x := f + 1; x < maxX; x++ {
					if matrix[y][x] == 1 {
						matrix[y][x], matrix[y][f-(x-f)] = 0, 1
					}
				}
			}
			maxX = f
		} else {
			f = -f
			log.Debugln("Fold at y=", f)
			for y := f + 1; y < maxY; y++ {
				for x := 0; x < maxX; x++ {
					if matrix[y][x] == 1 {
						matrix[y][x], matrix[f-(y-f)][x] = 0, 1
					}
				}
			}
			maxY = f
		}
	}
	count := uint(0)
	for y := 0; y < maxY; y++ {
		for x := 0; x < maxX; x++ {
			if matrix[y][x] == 1 {
				count++
				os.Stderr.Write([]byte{'#'})
			} else {
				os.Stderr.Write([]byte{'.'})
			}
		}
		os.Stderr.Write([]byte{'\n'})
	}
	log.Debugln("Count empties:", count)
}

func readInput(in io.Reader) ([][]uint, []int) {
	reader := bufio.NewReader(in)
	coords, err := bufio_.ReadProcessBytesLinesFunc[[]uint](reader, '\n', func(line []byte) ([]uint, error) {
		if len(line) == 0 {
			return nil, bufio_.ErrProcessingCompleted
		}
		xbytes, ybytes, ok := bytes.Cut(line, []byte(","))
		if !ok {
			return nil, os.ErrInvalid
		}
		return []uint{strconv.MustParseUintDecimal[uint](string(xbytes)), strconv.MustParseUintDecimal[uint](string(ybytes))}, nil
	})
	assert.Success(err, "Failed to read data from input.")
	folds, err := bufio_.ReadProcessBytesLinesFunc[int](reader, '\n', func(line []byte) (int, error) {
		if len(line) == 0 {
			return 0, bufio_.ErrProcessingCompleted
		}
		if !bytes.HasPrefix(line, []byte("fold along ")) {
			return 0, os.ErrInvalid
		}
		fold := line[11:]
		switch {
		case bytes.HasPrefix(fold, []byte("x=")):
			return strconv.MustParseInt[int](string(fold[2:]), 10), nil
		case bytes.HasPrefix(fold, []byte("y=")):
			return -strconv.MustParseInt[int](string(fold[2:]), 10), nil
		default:
			return 0, os.ErrInvalid
		}
	})
	assert.Success(err, "Failed to read folds from input")
	maxX := slices.Fold[[]uint, uint](coords, 0, func(r uint, v []uint) uint {
		return math.Max[uint](v[0], r)
	})
	maxY := uint(slices.Fold[[]uint, uint](coords, 0, func(r uint, v []uint) uint {
		return math.Max[uint](v[1], r)
	}))
	matrix := slices.Create2D[uint](maxX+1, maxY+1, 0)
	for _, c := range coords {
		matrix[c[1]][c[0]] = 1
	}
	return matrix, folds
}
