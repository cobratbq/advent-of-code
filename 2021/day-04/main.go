package main

import (
	"bufio"
	"fmt"
	"io"
	"strings"

	"github.com/cobratbq/goutils/assert"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	"github.com/cobratbq/goutils/std/builtin/set"
	"github.com/cobratbq/goutils/std/builtin/slices"
	"github.com/cobratbq/goutils/std/errors"
	io_ "github.com/cobratbq/goutils/std/io"
	"github.com/cobratbq/goutils/std/log"
	"github.com/cobratbq/goutils/std/strconv"
)

const BOARD_SIZE = ROW_SIZE * ROW_SIZE
const ROW_SIZE = 5

func main() {
	reader, closer, inputErr := bufio_.OpenFileReadOnly("input.txt")
	assert.Success(inputErr, "Failed to open input file.")
	defer io_.ClosePanicked(closer, "Failed to close input file.")

	draws := readDrawnNumbersLine(reader)
	log.Debugln("Bingo draws:", draws)
	fmt.Println()
	boards := readBoards(reader)

	winners := make(map[int]struct{})
game:
	for _, draw := range draws {
		log.Infoln("Next:", draw)
		for id, board := range boards {
			if _, present := winners[id]; present {
				continue
			}
			for i := 0; i < BOARD_SIZE; i++ {
				if board[i] == int(draw) {
					// NOTE: need to subtract 1 to ensure that 0 is properly signed negative when
					// marked
					board[i] = -board[i] - 1
				}
			}
			// Part 2: check winning status and print last board that wins.
			if checkWinner(board) {
				set.Insert(winners, id)
				log.Infoln("Winning board:", id+1)
				// NOTE: negative values are draw-number-minus-one to compensate for 0 issue.
				printBoard(board)
				score := scoreBoard(board, draw)
				log.Infoln("Score:", score)
			}
		}
		if len(winners) == len(boards) {
			break game
		}
	}
}

func readDrawnNumbersLine(reader *bufio.Reader) []uint {
	drawsLine, readErr := bufio_.ReadStringNoDelim(reader, '\n')
	assert.Success(readErr, "Failed to read drawn numbers line from input")
	numberStrings := strings.Split(drawsLine, ",")
	return slices.Transform(numberStrings, strconv.MustParseUintDecimal[uint])
}

func readBoards(reader *bufio.Reader) [][]int {
	var boards = make([][]int, 0)
	var current []int
	for {
		line, readErr := bufio_.ReadStringNoDelim(reader, '\n')
		if len(line) == 0 {
			// separator, one board is completely described.
			if cap(current) == BOARD_SIZE {
				assert.Equal(BOARD_SIZE, len(current))
				boards = append(boards, current)
			}
			current = make([]int, 0, BOARD_SIZE)
		} else {
			// parse one line of board composition
			numtexts := strings.Fields(line)
			assert.Equal(5, len(numtexts))
			for _, text := range numtexts {
				current = append(current, strconv.MustParseInt[int](text, 10))
			}
		}
		if errors.Is(readErr, io.EOF) {
			break
		}
		assert.Success(readErr, "Failed to read line from reader")
	}
	return boards
}

func printBoard(board []int) {
	for i := 0; i < ROW_SIZE; i++ {
		fmt.Printf("%3d %3d %3d %3d %3d\n", board[i*ROW_SIZE], board[i*ROW_SIZE+1],
			board[i*ROW_SIZE+2], board[i*ROW_SIZE+3], board[i*ROW_SIZE+4])
	}
}

func checkWinner(board []int) bool {
	for i := 0; i < ROW_SIZE; i++ {
		if board[i*ROW_SIZE] < 0 && board[i*ROW_SIZE+1] < 0 && board[i*ROW_SIZE+2] < 0 &&
			board[i*ROW_SIZE+3] < 0 && board[i*ROW_SIZE+4] < 0 {
			return true
		}
		if board[i+0*ROW_SIZE] < 0 && board[i+1*ROW_SIZE] < 0 && board[i+2*ROW_SIZE] < 0 &&
			board[i+3*ROW_SIZE] < 0 && board[i+4*ROW_SIZE] < 0 {
			return true
		}
	}
	return false
}

func scoreBoard(board []int, lastDraw uint) uint {
	var sum uint
	for _, num := range board {
		if num > 0 {
			sum += uint(num)
		}
	}
	return sum * lastDraw
}
