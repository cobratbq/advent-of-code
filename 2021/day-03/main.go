package main

import (
	"io"

	"github.com/cobratbq/goutils/assert"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	"github.com/cobratbq/goutils/std/errors"
	io_ "github.com/cobratbq/goutils/std/io"
	"github.com/cobratbq/goutils/std/log"
)

const BITLENGTH = 12

func main() {
	reader, closer, inputErr := bufio_.OpenFileReadOnly("input.txt")
	assert.Success(inputErr, "Failed to open input file")
	defer io_.ClosePanicked(closer, "Failed to gracefully close input file")
	var counts [BITLENGTH][2]uint16
	var data [][BITLENGTH]byte
	for {
		line, readErr := bufio_.ReadBytesNoDelim(reader, '\n')
		if errors.Is(readErr, io.EOF) {
			break
		}
		assert.Success(readErr, "Failed to read line of input")
		assert.Equal(len(counts), len(line))
		log.Debugln(string(line))
		var entry [BITLENGTH]byte
		for i := 0; i < len(line); i++ {
			switch line[i] {
			case '0':
				entry[i] = 0
				counts[i][0]++
			case '1':
				entry[i] = 1
				counts[i][1]++
			default:
				panic("Invalid input data")
			}
		}
		data = append(data, entry)
	}
	log.Debugf("%+v", counts)
	var gamma, epsilon uint16
	for i := 0; i < BITLENGTH; i++ {
		idx := BITLENGTH - i - 1
		if counts[i][1] >= counts[i][0] {
			gamma = gamma | (1 << idx)
		} else {
			epsilon = epsilon | (1 << idx)
		}
	}
	log.Infoln("Gamma:", gamma, ", epsilon:", epsilon, ", power:", uint32(gamma)*uint32(epsilon))
	log.Debugln("Oxygen generator rating:", filter(data, 0, false))
	log.Debugln("CO2 scrubber rating:", filter(data, 0, true))
}

func filter(data [][BITLENGTH]byte, idx uint, inverted bool) [BITLENGTH]byte {
	if len(data) == 1 {
		return data[0]
	}
	assert.Require(len(data) > 0, "Invalid input data.")
	var counts [2]uint
	for _, entry := range data {
		if entry[idx] == 0 {
			counts[0]++
		} else if entry[idx] == 1 {
			counts[1]++
		} else {
			panic("Illegal input")
		}
	}
	var bit byte
	if counts[1] >= counts[0] {
		bit = 1
	} else {
		bit = 0
	}
	if inverted {
		bit = 1 - bit
	}
	var dst = make([][BITLENGTH]byte, 0)
	for _, entry := range data {
		if entry[idx] != bit {
			continue
		}
		dst = append(dst, entry)
	}
	return filter(dst, idx+1, inverted)
}
