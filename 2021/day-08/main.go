package main

import (
	"strings"

	"github.com/cobratbq/goutils/assert"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	"github.com/cobratbq/goutils/std/builtin"
	"github.com/cobratbq/goutils/std/builtin/slices"
	"github.com/cobratbq/goutils/std/log"
	strings_ "github.com/cobratbq/goutils/std/strings"
)

func main() {
	data := builtin.Expect(readInput(inputFile))
	var total uint
	for _, entry := range data {
		table := analyze(entry.pattern)
		value := uint(slices.IndexFunc(table[:], strings_.IdenticalTo(entry.value[0]))*1000 +
			slices.IndexFunc(table[:], strings_.IdenticalTo(entry.value[1]))*100 +
			slices.IndexFunc(table[:], strings_.IdenticalTo(entry.value[2]))*10 +
			slices.IndexFunc(table[:], strings_.IdenticalTo(entry.value[3])))
		total += value
	}
	log.Infoln("Total:", total)
}

const inputFile = "input.txt"

func analyze(patterns [10]string) [10]string {
	var table [10]string
	length5 := make([]string, 0, 2)
	length6 := make([]string, 0, 3)
	for _, p := range patterns {
		if len(p) == 2 {
			table[1] = p
		} else if len(p) == 3 {
			table[7] = p
		} else if len(p) == 4 {
			table[4] = p
		} else if len(p) == 7 {
			table[8] = p
		} else if len(p) == 5 {
			length5 = append(length5, p)
		} else if len(p) == 6 {
			length6 = append(length6, p)
		}
	}
	table[3] = slices.Filter(length5, strings_.SupersetOf(table[7]))[0]
	table[6] = slices.Filter(length6, strings_.NotSupersetOf(table[7]))[0]
	table[5] = slices.Filter(length5, strings_.SubsetOf(table[6]))[0]
	table[2] = slices.Filter(length5, slices.NotContainedIn(table[:]))[0]
	table[9] = slices.Filter(length6, strings_.SupersetOf(table[4]))[0]
	table[0] = slices.Filter(length6, slices.NotContainedIn(table[:]))[0]
	return table
}

func readInput(name string) ([]entry, error) {
	return bufio_.OpenFileProcessStringLinesFunc(name, '\n', func(line string) (entry, error) {
		if len(line) == 0 {
			return entry{}, bufio_.ErrProcessingIgnore
		}
		patternPart, valuePart, ok := strings.Cut(line, " | ")
		assert.Require(ok, "Illegal format for line: missing separator between pattern and value")
		patterns := strings.Fields(patternPart)
		assert.Require(len(patterns) == 10, "Illegal format: incorrect number of pattern entries")
		values := strings.Fields(valuePart)
		assert.Require(len(values) == 4, "Illegal format: incorrect number of value entries")
		return entry{
			pattern: [10]string{patterns[0], patterns[1], patterns[2], patterns[3], patterns[4], patterns[5], patterns[6], patterns[7], patterns[8], patterns[9]},
			value:   [4]string{values[0], values[1], values[2], values[3]},
		}, nil
	})
}

type entry struct {
	pattern [10]string
	value   [4]string
}
