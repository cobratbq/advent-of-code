package main

import (
	"strings"

	"github.com/cobratbq/goutils/assert"
	"github.com/cobratbq/goutils/codec/number/interval"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	"github.com/cobratbq/goutils/std/builtin"
	"github.com/cobratbq/goutils/std/builtin/maps"
	"github.com/cobratbq/goutils/std/builtin/multiset"
	io_ "github.com/cobratbq/goutils/std/io"
	"github.com/cobratbq/goutils/std/log"
	"github.com/cobratbq/goutils/std/strconv"
)

func main() {
	input := builtin.Expect(readInput("input.txt"))
	area := plot(input)
	log.Debugln("Area:", area)
	overlapping := maps.Filter(area, func(k pos, v uint) bool { return v > 1 })
	log.Infoln("Overlap:", len(overlapping))
}

func plot(input []segment) map[pos]uint {
	area := make(map[pos]uint)
	for _, s := range input {
		if s.from.x == s.to.x {
			for _, i := range interval.ExpandClosed(s.from.y, s.to.y) {
				multiset.Insert(area, pos{s.from.x, i})
			}
		} else if s.from.y == s.to.y {
			for _, i := range interval.ExpandClosed(s.from.x, s.to.x) {
				multiset.Insert(area, pos{i, s.from.y})
			}
		} else {
			xlist := interval.ExpandClosed(s.from.x, s.to.x)
			ylist := interval.ExpandClosed(s.from.y, s.to.y)
			assert.Equal(len(xlist), len(ylist))
			for i := 0; i < len(xlist); i++ {
				multiset.Insert(area, pos{xlist[i], ylist[i]})
			}
		}
	}
	return area
}

func readInput(name string) ([]segment, error) {
	reader, closer, inputErr := bufio_.OpenFileReadOnly(name)
	assert.Success(inputErr, "Failed to open input file")
	defer io_.ClosePanicked(closer, "Failed to close input file")
	return bufio_.ReadProcessStringLinesFunc(reader, '\n', func(line string) (segment, error) {
		if line == "" {
			return segment{}, bufio_.ErrProcessingIgnore
		}
		return parseSegment(line)
	})
}

func parseSegment(line string) (segment, error) {
	fromText, toText, match := strings.Cut(line, " -> ")
	assert.Require(match, "Unexpected input line for segment")
	from := builtin.Expect(parsePos(fromText))
	to := builtin.Expect(parsePos(toText))
	return segment{from, to}, nil
}

func parsePos(text string) (pos, error) {
	xText, yText, match := strings.Cut(text, ",")
	assert.Require(match, "Unexpected format for position")
	x := strconv.MustParseInt[int](xText, 10)
	y := strconv.MustParseInt[int](yText, 10)
	return pos{int(x), int(y)}, nil
}

type segment struct {
	from pos
	to   pos
}

type pos struct {
	x int
	y int
}
