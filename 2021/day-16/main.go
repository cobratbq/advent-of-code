package main

import (
	"bufio"
	"io"
	"os"

	"github.com/cobratbq/goutils/assert"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
)

func readInput(in io.Reader) ([]int, error) {
	reader := bufio.NewReader(in)
	return bufio_.ReadProcessBytesLinesFunc[int](reader, '\n', func(line []byte) (int, error) {
		if len(line) == 0 {
			return 0, bufio_.ErrProcessingIgnore
		}
		return len(line), nil
	})
}

func main() {
	data, err := readInput(os.Stdin)
	assert.Success(err, "Failed to read input")
}
