package main

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"os"
	"strings"

	"github.com/cobratbq/goutils/assert"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	io_ "github.com/cobratbq/goutils/std/io"
	"github.com/cobratbq/goutils/std/log"
	"github.com/cobratbq/goutils/std/strconv"
)

func main() {
	reader, closer, inputErr := bufio_.OpenFileReadOnly("input.txt")
	assert.Success(inputErr, "Failed to open input file")
	defer io_.ClosePanicked(closer, "Failed to gracefully close input file")

	var depth uint64
	var location uint64
	var aim uint64
	for {
		cmd, readErr := readCommand(reader)
		if errors.Is(readErr, io.EOF) {
			break
		}
		assert.Success(readErr, "Failed to read command")
		switch cmd.direction {
		case FORWARD:
			location += cmd.value
			depth += cmd.value * aim
		case DOWN:
			aim += cmd.value
		case UP:
			aim -= cmd.value
		default:
			panic("Illegal command")
		}
	}
	fmt.Println("Location:", location, ", depth:", depth, "(", location*depth, ")")
}

func readCommand(reader *bufio.Reader) (command, error) {
	entry, err := reader.ReadString('\n')
	if err != nil {
		return command{}, err
	}
	part1, part2, ok := strings.Cut(strings.TrimRight(entry, "\r\n "), " ")
	if !ok {
		return command{}, os.ErrInvalid
	}
	log.Debugln(part1, part2)
	var direction = parseDirection(part1)
	var value = parseValue(part2)
	return command{direction: direction, value: value}, nil
}

type command struct {
	direction direction
	value     uint64
}

func parseDirection(direction string) direction {
	switch direction {
	case "forward":
		return FORWARD
	case "down":
		return DOWN
	case "up":
		return UP
	default:
		panic("Illegal input: direction")
	}
}

const (
	FORWARD direction = iota
	DOWN
	UP
)

type direction uint8

func parseValue(value string) uint64 {
	return strconv.MustParseUint[uint64](value, 10)
}
