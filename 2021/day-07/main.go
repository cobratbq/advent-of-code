package main

import (
	"strings"

	"github.com/cobratbq/goutils/assert"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	"github.com/cobratbq/goutils/std/builtin"
	"github.com/cobratbq/goutils/std/builtin/slices"
	io_ "github.com/cobratbq/goutils/std/io"
	"github.com/cobratbq/goutils/std/log"
	math_ "github.com/cobratbq/goutils/std/math"
	sort_ "github.com/cobratbq/goutils/std/sort"
	"github.com/cobratbq/goutils/std/strconv"
)

func main() {
	data := builtin.Expect(readInput(inputFile))
	sort_.Number(data)
	log.Infoln(analyzeBalance(data))
}

const inputFile = "input.txt"

func analyzeBalance(data []uint) (uint, uint) {
	assert.Require(sort_.IsSorted(data), "Expected data to be sorted")
	count := func(value uint, data []uint) uint {
		var sum uint
		for _, v := range data {
			for i := uint(1); i <= math_.Difference(value, v); i++ {
				sum += i
			}
		}
		return sum
	}
	median := len(data) / 2
	bestI, bestSum := len(data[:median]), count(data[median], data)
	for i := median; i >= 0; i-- {
		if data[i] == data[i-1] {
			continue
		}
		sum := count(data[i], data)
		if sum > bestSum {
			break
		}
		bestI, bestSum = i, sum
	}
	for i := median; i < len(data)-1; i++ {
		if data[i] == data[i+1] {
			continue
		}
		sum := count(data[i], data)
		if sum > bestSum {
			break
		}
		bestI, bestSum = i, sum
	}
	// fine-tune to values in-between two most viable elements
	bestV := data[bestI]
	for v := data[bestI]; ; v++ {
		sum := count(v, data)
		if sum > bestSum {
			break
		}
		bestV, bestSum = v, sum
	}
	for v := data[bestI]; ; v-- {
		sum := count(v, data)
		if sum > bestSum {
			break
		}
		bestV, bestSum = v, sum
	}
	return bestV, bestSum
}

func readInput(name string) ([]uint, error) {
	reader, closer, inputErr := bufio_.OpenFileReadOnly(name)
	defer io_.ClosePanicked(closer, "Failed to close input file")
	if inputErr != nil {
		return nil, inputErr
	}
	line, readErr := bufio_.ReadStringNoDelim(reader, '\n')
	if readErr != nil {
		return nil, readErr
	}
	return slices.Transform(strings.Split(line, ","), strconv.MustParseUintDecimal[uint]), nil
}
