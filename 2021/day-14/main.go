package main

import (
	"bufio"
	"bytes"
	"io"
	"os"

	"github.com/cobratbq/goutils/assert"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	"github.com/cobratbq/goutils/std/builtin/slices"
	"github.com/cobratbq/goutils/std/log"
)

func main() {
	template, mapping := readInput(os.Stdin)
	log.Debugln(template)
	log.Debugln(mapping)
	current := template
	for s := 0; s < 40; s++ {
		log.Debugln("Step", s)
		next := []byte{current[0]}
		for i := 0; i < len(current)-1; i++ {
			next = append(next, mapping[[2]byte{current[i], current[i+1]}])
			next = append(next, current[i+1])
		}
		current = next
	}
	counts := slices.DistinctElementCount[byte](current)
	log.Infoln(len(current), counts)
}

func readInput(in io.Reader) ([]byte, map[[2]byte]byte) {
	var err error
	reader := bufio.NewReader(in)
	var template []byte
	template, err = bufio_.ReadBytesNoDelim(reader, '\n')
	assert.Success(err, "Failed to read template line.")
	_, err = bufio_.ReadBytesNoDelim(reader, '\n')
	assert.Success(err, "Failed to read separator line.")
	mapping := make(map[[2]byte]byte)
	bufio_.ReadBytesLinesFunc(reader, '\n', func(line []byte) error {
		if len(line) == 0 {
			return nil
		}
		pair, insert, ok := bytes.Cut(line, []byte(" -> "))
		if !ok {
			return os.ErrInvalid
		}
		mapping[[2]byte{pair[0], pair[1]}] = insert[0]
		return nil
	})
	return template, mapping
}
