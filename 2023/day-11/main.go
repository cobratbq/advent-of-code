package main

import (
	"bufio"
	"io"
	"os"

	"github.com/cobratbq/goutils/assert"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	"github.com/cobratbq/goutils/std/builtin"
	slices_ "github.com/cobratbq/goutils/std/builtin/slices"
	"github.com/cobratbq/goutils/std/log"
	math_ "github.com/cobratbq/goutils/std/math"
)

func readInput(in io.Reader) ([][]byte, error) {
	reader := bufio.NewReader(in)
	return bufio_.ReadProcessBytesLinesFunc[[]byte](reader, '\n', func(line []byte) ([]byte, error) {
		if len(line) == 0 {
			return []byte{}, bufio_.ErrProcessingIgnore
		}
		return line, nil
	})
}

func extractGalaxies(grid [][]byte) [][2]uint {
	var galaxies [][2]uint
	for y := uint(0); y < uint(len(grid)); y++ {
		for x := uint(0); x < uint(len(grid[y])); x++ {
			if grid[y][x] == '#' {
				galaxies = append(galaxies, [2]uint{x, y})
			}
		}
	}
	return galaxies
}

func analyzeExpansion(data [][]byte) ([]uint, []uint) {
	colcorr := []uint{}
	for y := uint(0); y < uint(len(data)); y++ {
		var found bool
		for x := uint(0); x < uint(len(data[y])) && !found; x++ {
			if data[y][x] != '.' {
				found = true
			}
		}
		if !found {
			colcorr = append(colcorr, y)
		}
	}
	rowcorr := []uint{}
	for x := uint(0); x < uint(len(data)); x++ {
		var found bool
		for y := uint(0); y < uint(len(data[x])) && !found; y++ {
			if data[y][x] != '.' {
				found = true
			}
		}
		if !found {
			rowcorr = append(rowcorr, x)
		}
	}
	return rowcorr, colcorr
}

func expandGalaxies(galaxies [][2]uint, rowcorr, colcorr []uint) {
	const factor uint = 1_000_000
	for i := 0; i < len(galaxies); i++ {
		var expand uint
		for r := 0; r < len(rowcorr) && rowcorr[r] <= galaxies[i][0]; r++ {
			assert.Unequal(rowcorr[r], galaxies[i][0])
			expand += factor - 1
		}
		galaxies[i][0] += expand
		expand = 0
		for c := 0; c < len(colcorr) && colcorr[c] <= galaxies[i][1]; c++ {
			assert.Unequal(colcorr[c], galaxies[i][1])
			expand += factor - 1
		}
		galaxies[i][1] += expand
	}
}

func main() {
	grid, err := readInput(os.Stdin)
	assert.Success(err, "Failed to read input")
	galaxies := extractGalaxies(grid)
	rowcorr, colcorr := analyzeExpansion(grid)
	expandGalaxies(galaxies, rowcorr, colcorr)
	var distances []uint
	for first := 0; first < len(galaxies); first++ {
		for second := first + 1; second < len(galaxies); second++ {
			dist := math_.Difference(galaxies[first][0], galaxies[second][0]) + math_.Difference(galaxies[first][1], galaxies[second][1])
			log.Debugln("Galaxy", first+1, "and", second+1, "distance:", dist)
			distances = append(distances, dist)
		}
	}
	sum := slices_.Reduce(distances, builtin.Add)
	log.Infoln("Sum:", sum)
}
