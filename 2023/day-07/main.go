package main

import (
	"bufio"
	"bytes"
	"io"
	"os"
	"sort"

	"github.com/cobratbq/goutils/assert"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	"github.com/cobratbq/goutils/std/builtin/multiset"
	slices_ "github.com/cobratbq/goutils/std/builtin/slices"
	"github.com/cobratbq/goutils/std/log"
	math_ "github.com/cobratbq/goutils/std/math"
	"github.com/cobratbq/goutils/std/strconv"
)

type score struct {
	pairs        []uint8
	threeOfAKind uint8
	fullHouse    uint8
	fourOfAKind  uint8
	fiveOfAKind  uint8
}

func scoreCard(card byte) uint8 {
	switch card {
	case '2':
		return 2
	case '3':
		return 3
	case '4':
		return 4
	case '5':
		return 5
	case '6':
		return 6
	case '7':
		return 7
	case '8':
		return 8
	case '9':
		return 9
	case 'T':
		return 10
	case 'J':
		return 1
	case 'Q':
		return 12
	case 'K':
		return 13
	case 'A':
		return 14
	default:
		panic("bad input")
	}
}

func scoreHand(hand [5]byte) score {
	var s score
	counts := slices_.DistinctElementCount(hand[:])
	elements := multiset.ElementsOrdered(counts, func(k1, k2 byte) bool {
		return counts[k1] > counts[k2]
	})
	used := uint(0)
	for _, card := range elements {
		if counts[card] == 5 {
			s.fiveOfAKind = math_.Max(s.fiveOfAKind, scoreCard(card))
		} else if counts[card] == 4 {
			s.fourOfAKind = math_.Max(s.fourOfAKind, scoreCard(card))
		} else if counts[card] == 3 && len(counts) == 2 {
			s.fullHouse = scoreCard(card)
		} else if counts[card] == 3 {
			s.threeOfAKind = math_.Max(s.threeOfAKind, scoreCard(card))
		} else if counts[card] == 2 && len(counts) == 2 {
			// prefer the triple over the pair in the full-house, for the indication
		} else if counts[card] == 2 {
			s.pairs = append(s.pairs, scoreCard(card))
		}
		if card == 'J' || counts['J']-used == 0 {
			continue
		}
		// For all cards that are not jokers, let the jokers contribute.
		if counts[card]+counts['J']-used >= 5 {
			s.fiveOfAKind = math_.Max(s.fiveOfAKind, scoreCard(card))
			used += 5 - counts[card]
		} else if counts[card]+counts['J']-used >= 4 {
			s.fourOfAKind = math_.Max(s.fourOfAKind, scoreCard(card))
			used += 4 - counts[card]
		} else if len(counts) == 3 && counts[card] == 2 && counts['J'] == 1 && counts[card]+counts['J']-used == 3 {
			s.fullHouse = math_.Max(s.fullHouse, scoreCard(card))
			used += 3 - counts[card]
		} else if counts[card]+counts['J']-used >= 3 {
			s.threeOfAKind = math_.Max(s.threeOfAKind, scoreCard(card))
			used += 3 - counts[card]
		} else if counts[card]+counts['J']-used >= 2 {
			s.pairs = append(s.pairs, scoreCard(card))
			used += 2 - counts[card]
		}
	}
	return s
}

func lesserCard(hand1 [5]byte, hand2 [5]byte) bool {
	for i := 0; i < 5; i++ {
		score := int(scoreCard(hand1[i])) - int(scoreCard(hand2[i]))
		if score == 0 {
			continue
		}
		return score < 0
	}
	return false
}

func main() {
	data, err := readInput(os.Stdin)
	assert.Success(err, "Failed to read input")
	for i := 0; i < len(data); i++ {
		data[i].score = scoreHand(data[i].hand)
	}
	sort.Slice(data, func(i, j int) bool {
		// Note: I am, in no way, "proud" of this, but it passes the "if it works, it works" test :-P
		if data[i].score.fiveOfAKind != 0 {
			if data[j].score.fiveOfAKind == 0 {
				return false
			}
			return lesserCard(data[i].hand, data[j].hand)
		} else if data[j].score.fiveOfAKind != 0 {
			return true
		}
		if data[i].score.fourOfAKind != 0 {
			if data[j].score.fourOfAKind == 0 {
				return false
			}
			return lesserCard(data[i].hand, data[j].hand)
		} else if data[j].score.fourOfAKind != 0 {
			return true
		}
		if data[i].score.fullHouse != 0 {
			if data[j].score.fullHouse == 0 {
				return false
			}
			return lesserCard(data[i].hand, data[j].hand)
		} else if data[j].score.fullHouse != 0 {
			return true
		}
		if data[i].score.threeOfAKind != 0 {
			if data[j].score.threeOfAKind == 0 {
				return false
			}
			return lesserCard(data[i].hand, data[j].hand)
		} else if data[j].score.threeOfAKind != 0 {
			return true
		}
		if len(data[i].score.pairs) >= 2 && len(data[j].score.pairs) >= 2 {
			return lesserCard(data[i].hand, data[j].hand)
		}
		if len(data[i].score.pairs) == len(data[j].score.pairs) {
			return lesserCard(data[i].hand, data[j].hand)
		}
		return len(data[i].score.pairs) < len(data[j].score.pairs)
	})
	for i, e := range data {
		log.Debugln("player", i, string(e.hand[:]), e.bid, e.score)
	}
	total := slices_.FoldIndexed(data, 0, func(v uint, i int, p player) uint {
		return v + uint(i+1)*p.bid
	})
	log.Infoln("Total:", total)
}

type player struct {
	hand  [5]byte
	bid   uint
	score score
}

func readInput(in io.Reader) ([]player, error) {
	reader := bufio.NewReader(in)
	return bufio_.ReadProcessBytesLinesFunc(reader, '\n', func(line []byte) (player, error) {
		if len(line) == 0 {
			return player{}, bufio_.ErrProcessingIgnore
		}
		fields := bytes.Fields(line)
		assert.Equal(2, len(fields))
		assert.Equal(5, len(fields[0]))
		return player{hand: [5]byte{fields[0][0], fields[0][1], fields[0][2], fields[0][3], fields[0][4]},
			bid: strconv.MustParseBytesUintDecimal[uint](fields[1])}, nil
	})
}
