package main

import (
	"bufio"
	"io"
	"os"
	"regexp"
	"strings"

	"github.com/cobratbq/goutils/assert"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	"github.com/cobratbq/goutils/std/builtin"
	"github.com/cobratbq/goutils/std/builtin/slices"
	log "github.com/cobratbq/goutils/std/log"
	math_ "github.com/cobratbq/goutils/std/math"
	"github.com/cobratbq/goutils/std/strconv"
)

func main() {
	games, err := readInput(os.Stdin)
	assert.Success(err, "Failed to read input")
	var maxRed, maxGreen, maxBlue uint = 12, 13, 14
	validGames := slices.Filter[game_t](games, func(game game_t) bool {
		for _, reveal := range game.reveals {
			if reveal.blue > maxBlue || reveal.green > maxGreen || reveal.red > maxRed {
				return false
			}
		}
		return true
	})
	sumValidGames := slices.Fold[game_t, uint](validGames, 0, func(sum uint, game game_t) uint {
		return sum + game.id
	})
	log.Infoln("Sum of possible games:", sumValidGames)
	powers := slices.Transform[game_t, uint](games, func(game game_t) uint {
		var minRed, minGreen, minBlue uint
		for _, reveal := range game.reveals {
			minRed = math_.Max[uint](minRed, reveal.red)
			minGreen = math_.Max[uint](minGreen, reveal.green)
			minBlue = math_.Max[uint](minBlue, reveal.blue)
		}
		return minRed * minGreen * minBlue
	})
	sumPowers := slices.Fold[uint](powers, 0, builtin.Add[uint])
	log.Infoln("Sum of powers of minima:", sumPowers)
}

// pattern matches the "Game <id>:" prefix and then the totality of the games. Then we manually cut
// the games-description in parts.
var pattern = regexp.MustCompile(`Game (\d+):((?:(?: ?\d+ (?:red|green|blue),?)+;?)+)`)

type reveal_t struct {
	red   uint
	green uint
	blue  uint
}

type game_t struct {
	id      uint
	reveals []reveal_t
}

func readInput(in io.Reader) ([]game_t, error) {
	reader := bufio.NewReader(in)
	return bufio_.ReadProcessStringLinesFunc[game_t](reader, '\n', func(line string) (game_t, error) {
		if len(line) == 0 {
			return game_t{}, bufio_.ErrProcessingIgnore
		}
		if !pattern.MatchString(line) {
			log.Debugln("Non-matching:", line)
			return game_t{}, os.ErrInvalid
		}
		matches := pattern.FindStringSubmatch(line)
		var game game_t
		game.id = strconv.MustParseUintDecimal[uint](matches[1])
		for _, reveal := range strings.Split(matches[2], ";") {
			var red, green, blue uint
			for _, show := range strings.Split(reveal, ",") {
				show = strings.TrimLeft(show, " ")
				count, color, _ := strings.Cut(show, " ")
				switch color {
				case "red":
					red = strconv.MustParseUintDecimal[uint](count)
				case "green":
					green = strconv.MustParseUintDecimal[uint](count)
				case "blue":
					blue = strconv.MustParseUintDecimal[uint](count)
				default:
					panic("Unsupported color or bug")
				}
			}
			game.reveals = append(game.reveals, reveal_t{red: red, blue: blue, green: green})
		}
		log.Debugln("Game:", game)
		return game, nil
	})
}
