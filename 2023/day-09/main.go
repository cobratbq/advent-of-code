package main

import (
	"bufio"
	"bytes"
	"io"
	"os"

	"github.com/cobratbq/goutils/assert"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	"github.com/cobratbq/goutils/std/builtin"
	slices_ "github.com/cobratbq/goutils/std/builtin/slices"
	"github.com/cobratbq/goutils/std/log"
	strconv_ "github.com/cobratbq/goutils/std/strconv"
)

func readInput(in io.Reader) ([][]int, error) {
	reader := bufio.NewReader(in)
	return bufio_.ReadProcessBytesLinesFunc[[]int](reader, '\n', func(line []byte) ([]int, error) {
		if len(line) == 0 {
			return nil, bufio_.ErrProcessingIgnore
		}
		parts := slices_.Transform[[]byte, int](bytes.Fields(line), strconv_.MustParseBytesIntDecimal)
		return parts, nil
	})
}

func solveDeriveNext(line []int) int {
	inter := [][]int{}
	current := line
	for !slices_.All(current, builtin.Zero) {
		deriv := make([]int, len(current)-1)
		for i := 0; i < len(current)-1; i += 1 {
			deriv[i] = current[i+1] - current[i]
		}
		inter = append(inter, current)
		current = deriv
	}
	log.Debugln("Intermediates:", inter)
	var next int
	for i := len(inter) - 1; i >= 0; i -= 1 {
		next = inter[i][len(inter[i])-1] + next
		log.Debugln("next:", next)
	}
	return next
}

func main() {
	lines, err := readInput(os.Stdin)
	assert.Success(err, "Failed to read input")
	var cum int
	for i, l := range lines {
		next := solveDeriveNext(l)
		log.Infoln("Line:", i, "next:", next)
		cum += next
	}
	log.Infoln("Cumulative (right):", cum)
	cum = 0
	for i, l := range lines {
		next := solveDeriveNext(slices_.Reversed(l))
		log.Infoln("Line:", i, "next:", next)
		cum += next
	}
	log.Infoln("Cumulative (left):", cum)
}
