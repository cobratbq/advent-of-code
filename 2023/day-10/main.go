package main

import (
	"bufio"
	"io"
	"os"

	"github.com/cobratbq/goutils/assert"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	slices_ "github.com/cobratbq/goutils/std/builtin/slices"
	"github.com/cobratbq/goutils/std/log"
)

type coord struct {
	x int
	y int
}

type maze struct {
	input    [][]uint8
	start    coord
	analyzed [][]int16
}

func readInput(in io.Reader) (maze, error) {
	reader := bufio.NewReader(in)
	input, err := bufio_.ReadProcessBytesLinesFunc[[]uint8](reader, '\n', func(line []uint8) ([]uint8, error) {
		if len(line) == 0 {
			return []byte{}, bufio_.ErrProcessingIgnore
		}
		return line, nil
	})
	if err != nil {
		return maze{}, os.ErrInvalid
	}
	var maze maze
	maze.input = input
	maze.analyzed = slices_.Create2D[int16](uint(len(input[0])), uint(len(input)), -1)
	for y := 0; y < len(input); y += 1 {
		for x := 0; x < len(input[y]); x += 1 {
			if input[y][x] == 'S' {
				assert.Equal(coord{0, 0}, maze.start)
				maze.start = coord{x, y}
			}
		}
	}
	return maze, nil
}

func determineDirection(maze maze) {
	left := maze.start.x > 0
	top := maze.start.y > 0
	right := maze.start.x+1 < len(maze.input[maze.start.y])
	bottom := maze.start.y+1 < len(maze.input)
	left = left && (maze.input[maze.start.y][maze.start.x-1] == '-' || maze.input[maze.start.y][maze.start.x-1] == 'L' || maze.input[maze.start.y][maze.start.x-1] == 'F')
	right = right && (maze.input[maze.start.y][maze.start.x+1] == '-' || maze.input[maze.start.y][maze.start.x+1] == 'J' || maze.input[maze.start.y][maze.start.x+1] == '7')
	top = top && (maze.input[maze.start.y-1][maze.start.x] == '|' || maze.input[maze.start.y-1][maze.start.x] == 'F' || maze.input[maze.start.y-1][maze.start.x] == '7')
	bottom = bottom && (maze.input[maze.start.y+1][maze.start.x] == '|' || maze.input[maze.start.y+1][maze.start.x] == 'J' || maze.input[maze.start.y+1][maze.start.x] == 'L')
	var count uint8
	if left {
		count += 1
	}
	if top {
		count += 1
	}
	if right {
		count += 1
	}
	if bottom {
		count += 1
	}
	assert.Equal(2, count)
	if left && top {
		maze.input[maze.start.y][maze.start.x] = 'J'
	} else if top && right {
		maze.input[maze.start.y][maze.start.x] = 'L'
	} else if bottom && right {
		maze.input[maze.start.y][maze.start.x] = 'F'
	} else if bottom && left {
		maze.input[maze.start.y][maze.start.x] = '7'
	} else {
		panic("This should be unreachable.")
	}
}

func traceMeasureLoop(maze maze) {
	var depth int16
	maze.analyzed[maze.start.y][maze.start.x] = depth
	type nextType struct {
		from    coord
		current coord
	}
	next := [2]nextType{}
	switch maze.input[maze.start.y][maze.start.x] {
	case '-':
		next[0] = nextType{coord{maze.start.x, maze.start.y}, coord{maze.start.x - 1, maze.start.y}}
		next[1] = nextType{coord{maze.start.x, maze.start.y}, coord{maze.start.x + 1, maze.start.y}}
	case '|':
		next[0] = nextType{coord{maze.start.x, maze.start.y}, coord{maze.start.x, maze.start.y - 1}}
		next[1] = nextType{coord{maze.start.x, maze.start.y}, coord{maze.start.x, maze.start.y + 1}}
	case 'F':
		next[0] = nextType{coord{maze.start.x, maze.start.y}, coord{maze.start.x + 1, maze.start.y}}
		next[1] = nextType{coord{maze.start.x, maze.start.y}, coord{maze.start.x, maze.start.y + 1}}
	case '7':
		next[0] = nextType{coord{maze.start.x, maze.start.y}, coord{maze.start.x - 1, maze.start.y}}
		next[1] = nextType{coord{maze.start.x, maze.start.y}, coord{maze.start.x, maze.start.y + 1}}
	case 'J':
		next[0] = nextType{coord{maze.start.x, maze.start.y}, coord{maze.start.x - 1, maze.start.y}}
		next[1] = nextType{coord{maze.start.x, maze.start.y}, coord{maze.start.x, maze.start.y - 1}}
	case 'L':
		next[0] = nextType{coord{maze.start.x, maze.start.y}, coord{maze.start.x + 1, maze.start.y}}
		next[1] = nextType{coord{maze.start.x, maze.start.y}, coord{maze.start.x, maze.start.y - 1}}
	default:
		panic("Unsupported case")
	}
	var maxDepth int16
	for i := uint(0); ; i += 1 {
		idx := int(i % 2)
		if maze.analyzed[next[idx].current.y][next[idx].current.x] >= 0 {
			break
		}
		maxDepth = int16(i/2) + 1
		maze.analyzed[next[idx].current.y][next[idx].current.x] = int16(i/2) + 1
		deltaX := next[idx].current.x - next[idx].from.x
		deltaY := next[idx].current.y - next[idx].from.y
		switch maze.input[next[idx].current.y][next[idx].current.x] {
		case '-', '|':
			next[idx] = nextType{next[idx].current, coord{next[idx].current.x + deltaX, next[idx].current.y + deltaY}}
		case 'F', 'J':
			next[idx] = nextType{next[idx].current, coord{next[idx].current.x - deltaY, next[idx].current.y - deltaX}}
		case '7', 'L':
			next[idx] = nextType{next[idx].current, coord{next[idx].current.x + deltaY, next[idx].current.y + deltaX}}
		default:
			panic("Unsupported case")
		}
	}
	log.Infoln("Max depth:", maxDepth)
}

// FIXME this could probably be optimized/simplified, but the answer is correct, so I'll leave it at that for now.
func scanInnerOuterCells(maze maze) {
	for y := 0; y < len(maze.input); y += 1 {
		var inner bool
		var bend byte
		for x := 0; x < len(maze.input[y]); x += 1 {
			if maze.analyzed[y][x] < 0 {
				if inner {
					maze.input[y][x] = 'I'
				} else {
					maze.input[y][x] = 'O'
				}
				continue
			}
			switch maze.input[y][x] {
			case '-':
				continue
			case '.':
				if inner {
					maze.input[y][x] = 'I'
				} else {
					maze.input[y][x] = 'O'
				}
			case '|':
				inner = !inner
			case 'F', '7', 'J', 'L':
				if (bend == 'F' && maze.input[y][x] == 'J') || (bend == '7' && maze.input[y][x] == 'L') ||
					(bend == 'J' && maze.input[y][x] == 'F') || (bend == 'L' && maze.input[y][x] == '7') {
					inner = !inner
					bend = 0
				} else if (bend == 'F' && maze.input[y][x] == '7') || (bend == 'L' && maze.input[y][x] == 'J') ||
					(bend == '7' && maze.input[y][x] == 'F') || (bend == 'J' && maze.input[y][x] == 'L') {
					bend = 0
				} else {
					bend = maze.input[y][x]
				}
			default:
				panic("Unsupported case")
			}
		}
	}
	log.DebuglnSliceAsString("scan:", maze.input)
	log.Infoln("Inners:", slices_.Fold(maze.input, 0, func(i int, u []uint8) int {
		return i + slices_.Fold(u, 0, func(i int, u uint8) int {
			if u == 'I' {
				return i + 1
			} else {
				return i
			}
		})
	}))
}

func main() {
	data, err := readInput(os.Stdin)
	assert.Success(err, "Failed to read input")
	determineDirection(data)
	traceMeasureLoop(data)
	scanInnerOuterCells(data)
}
