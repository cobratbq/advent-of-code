package main

import (
	"bufio"
	"bytes"
	"io"
	"os"

	"github.com/cobratbq/goutils/assert"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	"github.com/cobratbq/goutils/std/builtin"
	slices_ "github.com/cobratbq/goutils/std/builtin/slices"
	"github.com/cobratbq/goutils/std/log"
	math_ "github.com/cobratbq/goutils/std/math"
	strconv_ "github.com/cobratbq/goutils/std/strconv"
)

type spring struct {
	record  []byte
	entries []uint
}

func readInput(in io.Reader) ([]spring, error) {
	reader := bufio.NewReader(in)
	return bufio_.ReadProcessBytesLinesFunc(reader, '\n', func(line []byte) (spring, error) {
		if len(line) == 0 {
			return spring{}, bufio_.ErrProcessingIgnore
		}
		fields := bytes.Fields(line)
		if len(fields) != 2 {
			return spring{}, nil
		}
		var s spring
		s.record = fields[0]
		s.entries = slices_.Transform(bytes.Split(fields[1], []byte{','}), strconv_.MustParseBytesUintDecimal[uint])
		return s, nil
	})
}

func stripFullSequences(record []byte, entries []uint) ([]byte, []uint) {
	var i int
	for e := range entries {
		for ; i < len(record) && record[i] == '#'; i++ {
		}
		if record[i] == '?' {
			entries = entries[e:]
			break
		}
		assert.Equal(uint(len(record[:i])), entries[e])
		for ; i < len(record) && record[i] == '.'; i++ {
		}
		record, i = record[i:], 0
	}
	i = len(record) - 1
	for e := len(entries) - 1; e >= 0; e-- {
		for ; i >= 0 && record[i] == '#'; i-- {
		}
		if record[i] == '?' {
			entries = entries[:e+1]
			break
		}
		assert.Equal(uint(len(record[i+1:])), entries[e])
		for ; i >= 0 && record[i] == '.'; i-- {
		}
		record = record[:i+1]
		entries = entries[:e]
	}
	return record, entries
}

func stripEstablishedParts(record []byte, entries []uint) ([]byte, []uint) {
	var reduced []byte
	reduced = bytes.TrimRight(bytes.TrimLeft(record, "."), ".")
	var remaining []uint
	reduced, remaining = stripFullSequences(reduced, entries)
	return reduced, remaining
}

func solveForEntry(record []byte, entry uint) uint {
	log.Debugln(entry, string(record))
	distinct := slices_.DistinctElementCount(record)
	assert.Equal(uint(len(record)), distinct['#']+distinct['?'])
	if distinct['#'] == 0 {
		return distinct['?'] + 1 - entry
	}
	if distinct['#'] == entry || distinct['#']+distinct['?'] == entry {
		return 1
	}
	assert.True(distinct['#']+distinct['?'] > entry)
	remaining := distinct['?'] - (uint(bytes.LastIndexByte(record, '#')-bytes.IndexByte(record, '#')+1) - distinct['#'])
	// FIXME determine nr of prefix candidates and suffix candidates while respecting fixed positions
	// ??#?? =^= 5
	// ??#?? =^= 4
	// ??#?? =^= 3
	// ??#?? =^= 2
	return math_.Min(remaining, uint(bytes.IndexByte(record, '#'))) + math_.Min(remaining, uint(len(record)-bytes.LastIndexByte(record, '#')))
}

func solveUnknowns(s spring) uint {
	reduced, entries := stripEstablishedParts(s.record, s.entries)
	log.Debugln("Reduced record:", string(reduced))
	log.DebuglnSlice("Entries:", entries)
	subrecords := bytes.FieldsFunc(reduced, builtin.EqualsFixedOf('.'))
	log.DebuglnSliceAsString("Subrecord:", subrecords)
	var count uint
	if len(subrecords) == len(entries) {
		// FIXME incorrect assumption: a series of '?' may just be '.', so we cannot assume that each subrecord corresponds to an entry
		for i := 0; i < len(subrecords); i++ {
			assert.True(len(subrecords[i]) >= int(entries[i]))
			count += solveForEntry(subrecords[i], entries[i])
		}
	} else {
		// FIXME asymmetry between records and entries, need to solve from left or right
	}
	// FIXME solve prefix/suffix by necessity for given positions of '#', e.g. first entry value `1` implies that '#?' ? must be '.'
	// FIXME solve remaining entries
	// FIXME solve for each predetermined subsection
	return count
}

func main() {
	springs, err := readInput(os.Stdin)
	assert.Success(err, "Failed to read input")
	var count uint
	for _, s := range springs {
		count += solveUnknowns(s)
	}
	log.Infoln("Total possibilities:", count)
}
