package main

import (
	"bufio"
	"io"
	"os"
	"sort"
	"strings"

	"github.com/cobratbq/goutils/assert"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	"github.com/cobratbq/goutils/std/builtin"
	slices_ "github.com/cobratbq/goutils/std/builtin/slices"
	"github.com/cobratbq/goutils/std/log"
	math_ "github.com/cobratbq/goutils/std/math"
	"github.com/cobratbq/goutils/std/strconv"
	"github.com/cobratbq/goutils/structure/interval"
	"github.com/cobratbq/goutils/types"
)

func main() {
	data, err := readInput(os.Stdin)
	assert.Success(err, "Failed to read input")
	var lowest = types.MaxUint
	for _, seed := range data.seeds {
		log.Debugln("Seed:", seed)
		v := lookup(seed, data.seedToSoil, data.soilToFertilizer, data.fertilizerToWater,
			data.waterToLight, data.lightToTemp, data.tempToHumidity, data.humidityToLocation)
		lowest = math_.Min(lowest, v)
	}
	log.Infoln("Lowest location:", lowest)
	seedranges := parseSeeds(data.seeds)
	seedranges = splitOverlaps(seedranges, data.seedToSoil)
	seedranges = splitOverlaps(seedranges, data.soilToFertilizer)
	seedranges = splitOverlaps(seedranges, data.fertilizerToWater)
	seedranges = splitOverlaps(seedranges, data.waterToLight)
	seedranges = splitOverlaps(seedranges, data.lightToTemp)
	seedranges = splitOverlaps(seedranges, data.tempToHumidity)
	seedranges = splitOverlaps(seedranges, data.humidityToLocation)
	lowest = slices_.Fold(seedranges, types.MaxUint, func(r uint, e interval.UintInterval) uint {
		return math_.Min(r, e.Start())
	})
	log.Debugln()
	log.Debugln("Final ranges:", seedranges)
	log.Debugln("Lowest:", lowest)
}

func lookup(value uint, mappings ...[]mapping) uint {
mapping:
	for _, m := range mappings {
		for _, r := range m {
			if r.contains(value) {
				to := r.onto + (value - r.from.Start())
				log.Debugln("For", value, "range", r.from.Start(), r.onto, r.from.Size(), "maps to", to)
				value = to
				continue mapping
			}
		}
		log.Debugln("For", value, "maps to same value.")
	}
	return value
}

func splitOverlaps(original []interval.UintInterval, mapping []mapping) []interval.UintInterval {
	var next []interval.UintInterval
	next = []interval.UintInterval{}
interval:
	for _, p := range original {
		for _, m := range mapping {
			log.Debugln("Mapping:", m)
			if !m.from.Overlaps(&p) {
				log.Debugln("Pattern", p, "does not overlap", m.from)
				continue
			}
			if m.from.Contains(&p) {
				log.Debugln("Pattern", p, "contained in", m.from)
				next = append(next, interval.NewUint(m.onto+(p.Start()-m.from.Start()), p.Size()))
				continue interval
			}
			if p.Start() < m.from.Start() {
				// if there was a previous mapping, we would have already matched an overlap on that
				// mapping, so this must be an unmapped segment.
				next = append(next, interval.NewUint(p.Start(), m.from.Start()-p.Start()))
			}
			start := math_.Max(p.Start(), m.from.Start())
			next = append(next, interval.NewUint(m.onto+(start-m.from.Start()), math_.Min(m.from.End(), p.End())-start))
			if p.End() > m.from.End() {
				// if there is a trailing part after the mapping, continue with that as a reduced interval
				p = interval.NewUint(m.from.End(), p.End()-m.from.End())
			}
		}
		next = append(next, p)
	}
	log.Infoln("Ranges", len(next), ":", next)
	return next
}

type mapping struct {
	from interval.UintInterval
	onto uint
}

func (m mapping) contains(value uint) bool {
	// FIXME now is closed interval
	return m.from.ContainsValue(value)
}

func (m mapping) containsRange(value *interval.UintInterval) bool {
	// FIXME need to recheck
	return !(m.onto < value.Start() || m.from.Start() > value.End())
}

func sortMapping(data []mapping) []mapping {
	sort.Slice(data, func(i, j int) bool {
		return data[i].from.Start() < data[j].from.Start() || (data[i].from.Start() == data[j].from.Start() && data[i].from.Size() < data[j].from.Size())
	})
	return data
}

type almanac struct {
	seeds              []uint
	seedToSoil         []mapping
	soilToFertilizer   []mapping
	fertilizerToWater  []mapping
	waterToLight       []mapping
	lightToTemp        []mapping
	tempToHumidity     []mapping
	humidityToLocation []mapping
}

func readInput(in io.Reader) (almanac, error) {
	var result almanac
	reader := bufio.NewReader(in)
	seedsline, err := bufio_.ReadStringNoDelim(reader, '\n')
	assert.Success(err, "Failed to read seeds line from input.")
	assert.True(strings.HasPrefix(seedsline, "seeds: "))
	result.seeds = slices_.Transform(strings.Fields(seedsline[7:]), strconv.MustParseUintDecimal[uint])
	assert.Equal("", builtin.Expect(bufio_.ReadStringNoDelim(reader, '\n')))
	assert.Equal("seed-to-soil map:", builtin.Expect(bufio_.ReadStringNoDelim(reader, '\n')))
	result.seedToSoil = sortMapping(builtin.Expect(bufio_.ReadProcessStringLinesFunc(reader, '\n', readMapping)))
	assert.Equal("soil-to-fertilizer map:", builtin.Expect(bufio_.ReadStringNoDelim(reader, '\n')))
	result.soilToFertilizer = sortMapping(builtin.Expect(bufio_.ReadProcessStringLinesFunc(reader, '\n', readMapping)))
	assert.Equal("fertilizer-to-water map:", builtin.Expect(bufio_.ReadStringNoDelim(reader, '\n')))
	result.fertilizerToWater = sortMapping(builtin.Expect(bufio_.ReadProcessStringLinesFunc(reader, '\n', readMapping)))
	assert.Equal("water-to-light map:", builtin.Expect(bufio_.ReadStringNoDelim(reader, '\n')))
	result.waterToLight = sortMapping(builtin.Expect(bufio_.ReadProcessStringLinesFunc(reader, '\n', readMapping)))
	assert.Equal("light-to-temperature map:", builtin.Expect(bufio_.ReadStringNoDelim(reader, '\n')))
	result.lightToTemp = sortMapping(builtin.Expect(bufio_.ReadProcessStringLinesFunc(reader, '\n', readMapping)))
	assert.Equal("temperature-to-humidity map:", builtin.Expect(bufio_.ReadStringNoDelim(reader, '\n')))
	result.tempToHumidity = sortMapping(builtin.Expect(bufio_.ReadProcessStringLinesFunc(reader, '\n', readMapping)))
	assert.Equal("humidity-to-location map:", builtin.Expect(bufio_.ReadStringNoDelim(reader, '\n')))
	result.humidityToLocation = sortMapping(builtin.Expect(bufio_.ReadProcessStringLinesFunc(reader, '\n', readMapping)))
	return result, nil
}

func readMapping(line string) (mapping, error) {
	if len(line) == 0 {
		return mapping{}, bufio_.ErrProcessingCompleted
	}
	fields := strings.Fields(line)
	return mapping{
		from: interval.NewUint(strconv.MustParseUintDecimal[uint](fields[1]), strconv.MustParseUintDecimal[uint](fields[2])),
		onto: strconv.MustParseUintDecimal[uint](fields[0]),
	}, nil
}

func parseSeeds(seeds []uint) []interval.UintInterval {
	assert.Equal(0, len(seeds)%2)
	var ranges []interval.UintInterval
	for i := 0; i < len(seeds); i += 2 {
		ranges = append(ranges, interval.NewUint(seeds[i], seeds[i+1]))
	}
	log.Debugln("Ranges:", ranges)
	return ranges
}
