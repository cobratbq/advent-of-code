package main

import (
	"bufio"
	"io"
	"os"

	"github.com/cobratbq/goutils/assert"
	"github.com/cobratbq/goutils/codec/bytes/digit"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	"github.com/cobratbq/goutils/std/builtin"
	"github.com/cobratbq/goutils/std/builtin/maps"
	"github.com/cobratbq/goutils/std/builtin/multiset"
	"github.com/cobratbq/goutils/std/builtin/set"
	"github.com/cobratbq/goutils/std/log"
	"github.com/cobratbq/goutils/std/strconv"
)

type pos struct {
	x int
	y int
}

func main() {
	schematic, err := readInput(os.Stdin)
	assert.Success(err, "Failed to read input")
	parts := processSymbols(schematic)
	log.Infoln(parts)
	sum := maps.Fold[uint, uint, uint](parts, 0, func(r, key, value uint) uint {
		return r + key*value
	})
	log.Infoln("Sum:", sum)
	gears := processGears(schematic)
	sum = maps.FoldValues[pos, uint, uint](gears, 0, builtin.Add[uint])
	log.Infoln("Sum of gear ratios:", sum)
}

func processSymbols(schematic map[pos]byte) map[uint]uint {
	var parts = make(map[uint]uint)
	for p, c := range schematic {
		if digit.IsDigit(c) {
			continue
		}
		// collect parts in set first, to ensure that we don't count same part twice.
		finds := findParts(schematic, p)
		multiset.InsertSet(parts, finds)
	}
	return parts
}

func processGears(schematic map[pos]byte) map[pos]uint {
	var gears = make(map[pos]uint)
	for p, c := range schematic {
		if c != '*' {
			continue
		}
		// collect parts in set first, to ensure that we don't count same part twice.
		finds := findParts(schematic, p)
		if len(finds) != 2 {
			continue
		}
		parts := maps.ExtractKeys[uint](finds)
		gears[p] = parts[0] * parts[1]
	}
	return gears
}

func findParts(schematic map[pos]byte, p pos) map[uint]struct{} {
	var parts = make(map[uint]struct{})
	if digit.IsDigit(schematic[pos{p.x - 1, p.y - 1}]) {
		set.Insert[uint](parts, fullNumber(schematic, pos{p.x - 1, p.y - 1}))
	}
	if digit.IsDigit(schematic[pos{p.x, p.y - 1}]) {
		set.Insert[uint](parts, fullNumber(schematic, pos{p.x, p.y - 1}))
	}
	if digit.IsDigit(schematic[pos{p.x + 1, p.y - 1}]) {
		set.Insert[uint](parts, fullNumber(schematic, pos{p.x + 1, p.y - 1}))
	}
	if digit.IsDigit(schematic[pos{p.x - 1, p.y}]) {
		set.Insert[uint](parts, fullNumber(schematic, pos{p.x - 1, p.y}))
	}
	if digit.IsDigit(schematic[pos{p.x + 1, p.y}]) {
		set.Insert[uint](parts, fullNumber(schematic, pos{p.x + 1, p.y}))
	}
	if digit.IsDigit(schematic[pos{p.x - 1, p.y + 1}]) {
		set.Insert[uint](parts, fullNumber(schematic, pos{p.x - 1, p.y + 1}))
	}
	if digit.IsDigit(schematic[pos{p.x, p.y + 1}]) {
		set.Insert[uint](parts, fullNumber(schematic, pos{p.x, p.y + 1}))
	}
	if digit.IsDigit(schematic[pos{p.x + 1, p.y + 1}]) {
		set.Insert[uint](parts, fullNumber(schematic, pos{p.x + 1, p.y + 1}))
	}
	return parts
}

func fullNumber(schematic map[pos]byte, p pos) uint {
	assert.True(digit.IsDigit(schematic[p]))
	data := []byte{schematic[p]}
	for i := p.x - 1; i >= 0; i-- {
		if c, ok := schematic[pos{i, p.y}]; !ok || !digit.IsDigit(c) {
			break
		}
		data = append([]byte{schematic[pos{i, p.y}]}, data...)
	}
	for i := p.x + 1; ; i++ {
		if c, ok := schematic[pos{i, p.y}]; !ok || !digit.IsDigit(c) {
			break
		}
		data = append(data, schematic[pos{i, p.y}])
	}
	return strconv.MustParseUintDecimal[uint](string(data))
}

func readInput(in io.Reader) (map[pos]byte, error) {
	reader := bufio.NewReader(in)
	schematic := make(map[pos]byte)
	var err error
	var line []byte
	for y := 0; ; y++ {
		if line, err = bufio_.ReadBytesNoDelim(reader, '\n'); err != nil || len(line) == 0 {
			break
		}
		for x := 0; x < len(line); x++ {
			if line[x] == '.' {
				continue
			}
			schematic[pos{x, y}] = line[x]
		}
	}
	return schematic, nil
}
