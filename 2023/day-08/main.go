package main

import (
	"bufio"
	"io"
	"os"
	"regexp"

	bufio_ "github.com/cobratbq/goutils/std/bufio"
	"github.com/cobratbq/goutils/std/builtin"
	maps_ "github.com/cobratbq/goutils/std/builtin/maps"
	slices_ "github.com/cobratbq/goutils/std/builtin/slices"
	"github.com/cobratbq/goutils/std/log"
	math_ "github.com/cobratbq/goutils/std/math"
)

// `AAA = (BBB, CCC)`
var pattern = regexp.MustCompile(`([A-Z]+) = \(([A-Z]+), ([A-Z]+)\)`)

var start = [3]byte{'A', 'A', 'A'}
var end = [3]byte{'Z', 'Z', 'Z'}

type branch struct {
	left  [3]byte
	right [3]byte
}

func readInput(in io.Reader) ([]byte, map[[3]byte]branch) {
	reader := bufio.NewReader(in)
	directions := builtin.Expect(bufio_.ReadBytesNoDelim(reader, '\n'))
	_ = builtin.Expect(bufio_.ReadBytesNoDelim(reader, '\n'))
	type node struct {
		dst   [3]byte
		left  [3]byte
		right [3]byte
	}
	nodes := builtin.Expect(bufio_.ReadProcessBytesLinesFunc[node](reader, '\n', func(line []byte) (node, error) {
		if len(line) == 0 {
			return node{}, bufio_.ErrProcessingIgnore
		}
		match := pattern.FindSubmatch(line)
		if match == nil {
			return node{}, os.ErrInvalid
		}
		var node node
		node.dst = [3]byte(match[1])
		node.left = [3]byte(match[2])
		node.right = [3]byte(match[3])
		return node, nil
	}))
	network := make(map[[3]byte]branch, len(nodes))
	for _, n := range nodes {
		network[n.dst] = branch{n.left, n.right}
	}
	return directions, network
}

func main() {
	directions, network := readInput(os.Stdin)
	// log.Debugln(directions, network)
	type pos struct {
		step int
		loc  [3]byte
	}
	var current [][3]byte = slices_.Filter(maps_.ExtractKeys(network), func(v [3]byte) bool { return v[2] == 'A' })
	steps := 0
	counts := make([]uint, len(current))
	endings := make([]map[pos]uint, len(current))
	for i := 0; i < len(endings); i++ {
		endings[i] = make(map[pos]uint, 3)
	}
	for ; ; steps += 1 {
		direction := directions[steps%len(directions)]
		// log.Debugln("Current:", current, "next:", string(direction))
		switch direction {
		case 'L':
			slices_.Update(current, func(p [3]byte) [3]byte {
				return network[p].left
			})
		case 'R':
			slices_.Update(current, func(p [3]byte) [3]byte {
				return network[p].right
			})
		default:
			panic("Illegal direction")
		}
		for i, l := range current {
			newpos := pos{steps % len(directions), l}
			counts[i]++
			if newpos.loc[2] == 'Z' {
				endings[i][newpos] = counts[i]
			}
		}
		if slices_.All(endings, func(l map[pos]uint) bool { return len(l) > 0 }) {
			// When all parallel tracks have an ending, calculate the LCM for which all end-states are reached
			// simultaneously.
			lcm := slices_.Fold[map[pos]uint, uint](endings, 1, func(init uint, ending map[pos]uint) uint {
				return math_.LCM(init, maps_.FoldValues(ending, 1, func(init_ uint, count uint) uint {
					return math_.LCM(init_, count)
				}))
			})
			log.Debugln("LCM:", lcm)
			break
		}
	}
	log.Infoln("Steps:", steps+1)
}
