package main

import (
	"bufio"
	"bytes"
	"io"
	"os"
	"regexp"

	"github.com/cobratbq/goutils/assert"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	"github.com/cobratbq/goutils/std/builtin/multiset"
	"github.com/cobratbq/goutils/std/builtin/slices"
	"github.com/cobratbq/goutils/std/log"
	"github.com/cobratbq/goutils/std/strconv"
)

func main() {
	games, err := readInput(os.Stdin)
	assert.Success(err, "Failed to read input")
	sum := 0
	for _, g := range games {
		count := 0
		for _, winner := range g.winning {
			if slices.Contains[uint](g.numbers, winner) {
				count++
			}
		}
		if count > 0 {
			sum += 1 << (count - 1)
		}
		log.Debugln(g.id, g.winning, g.numbers)
	}
	log.Infoln("Sum:", sum)
	owned := make(map[uint]uint)
	for _, g := range games {
		log.Debugln(g.id, owned[g.id])
		multiset.Insert(owned, g.id)
		if owned[g.id] == 0 {
			break
		}
		count := uint(0)
		for _, winner := range g.winning {
			if slices.Contains[uint](g.numbers, winner) {
				count++
			}
		}
		for n := uint(1); n <= count; n++ {
			// FIXME need to avoid adding numbers for games that do not exist.
			multiset.InsertN(owned, g.id+n, owned[g.id])
		}
	}
	log.Debugln(owned)
	log.Infoln("Total:", multiset.Count[uint](owned))
}

var GameLine = regexp.MustCompile(`Card +(\d+):((?: +\d+)+) \|((?: +\d+)+)`)

type game_t struct {
	id      uint
	winning []uint
	numbers []uint
}

func readInput(in io.Reader) ([]game_t, error) {
	reader := bufio.NewReader(in)
	return bufio_.ReadProcessBytesLinesFunc[game_t](reader, '\n', func(line []byte) (game_t, error) {
		if len(line) == 0 {
			return game_t{}, bufio_.ErrProcessingIgnore
		}
		var matches [][]byte
		if matches = GameLine.FindSubmatch(line); matches == nil {
			log.Debugln("Bad line:", string(line))
			return game_t{}, os.ErrInvalid
		}
		cardId := strconv.MustParseUintDecimal[uint](string(matches[1]))
		winning := slices.Transform[[]byte, uint](bytes.Fields(matches[2]), strconv.MustParseBytesUintDecimal[uint])
		numbers := slices.Transform[[]byte, uint](bytes.Fields(matches[3]), strconv.MustParseBytesUintDecimal[uint])
		return game_t{cardId, winning, numbers}, nil
	})
}
