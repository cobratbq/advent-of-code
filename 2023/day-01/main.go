package main

import (
	"bufio"
	"bytes"
	"io"
	"os"

	"github.com/cobratbq/goutils/assert"
	"github.com/cobratbq/goutils/codec/bytes/digit"
	textdigit "github.com/cobratbq/goutils/codec/string/digit"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	"github.com/cobratbq/goutils/std/builtin"
	"github.com/cobratbq/goutils/std/builtin/maps"
	"github.com/cobratbq/goutils/std/builtin/slices"
	"github.com/cobratbq/goutils/std/log"
	sort_ "github.com/cobratbq/goutils/std/sort"
	"github.com/cobratbq/goutils/std/strconv"
)

func main() {
	values, err := readInputSecond(os.Stdin)
	assert.Success(err, "Failed to read input")
	log.Infoln("Sum:", slices.Fold(values, 0, builtin.Add[uint]))
}

func readInputFirst(in io.Reader) ([]uint, error) {
	reader := bufio.NewReader(in)
	var idx int
	return bufio_.ReadProcessBytesLinesFunc(reader, '\n', func(line []byte) (uint, error) {
		if len(line) == 0 {
			return 0, bufio_.ErrProcessingIgnore
		}
		var buffer []byte
		if idx = bytes.IndexFunc(line, digit.IsDigitRune); idx >= 0 {
			buffer = append(buffer, line[idx])
		}
		if idx = bytes.LastIndexFunc(line, digit.IsDigitRune); idx >= 0 {
			buffer = append(buffer, line[idx])
		}
		return strconv.MustParseUintDecimal[uint](string(buffer)), nil
	})
}

func readInputSecond(in io.Reader) ([]uint, error) {
	reader := bufio.NewReader(in)
	return bufio_.ReadProcessBytesLinesFunc(reader, '\n', func(line []byte) (uint, error) {
		if len(line) == 0 {
			return 0, bufio_.ErrProcessingIgnore
		}
		textFinds := textdigit.FindDigitOverlapping(textdigit.EnglishLowercase, string(line))
		log.Debugln("Textual finds:", len(textFinds))
		digitFinds := digit.FindDigit(line)
		log.Debugln("Digit finds:", len(digitFinds))
		finds := maps.Merge(textFinds, digitFinds)
		order := maps.ExtractKeys(finds)
		sort_.Number(order)
		buffer := []byte{digit.EncodeDigit(finds[order[0]]), digit.EncodeDigit(finds[order[len(order)-1]])}
		num := strconv.MustParseUintDecimal[uint](string(buffer))
		log.Debugln("Value:", num)
		return num, nil
	})
}
