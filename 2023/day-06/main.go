package main

import (
	"bufio"
	"io"
	"os"
	"strconv"
	"strings"

	"github.com/cobratbq/goutils/assert"
	bufio_ "github.com/cobratbq/goutils/std/bufio"
	"github.com/cobratbq/goutils/std/builtin"
	slices_ "github.com/cobratbq/goutils/std/builtin/slices"
	"github.com/cobratbq/goutils/std/log"
	strconv_ "github.com/cobratbq/goutils/std/strconv"
)

type race struct {
	time     uint
	distance uint
}

func readInput(in io.Reader) ([]race, error) {
	reader := bufio.NewReader(in)
	times := strings.Fields(builtin.Expect(bufio_.ReadStringNoDelim(reader, '\n')))[1:]
	distances := strings.Fields(builtin.Expect(bufio_.ReadStringNoDelim(reader, '\n')))[1:]
	assert.Equal(len(times), len(distances))
	var races []race
	for i := 0; i < len(times); i++ {
		races = append(races, race{time: strconv_.MustParseUintDecimal[uint](times[i]), distance: strconv_.MustParseUintDecimal[uint](distances[i])})
	}
	return races, nil
}

func countWinners(r *race) uint {
	count := uint(0)
	for c := uint(1); c < r.time-1; c++ {
		if c*(r.time-c) > r.distance {
			count++
		}
	}
	return count
}

func main() {
	races, err := readInput(os.Stdin)
	assert.Success(err, "Failed to read input")
	log.Debugln("Races:", races)
	var winners []uint
	for _, r := range races {
		winners = append(winners, countWinners(&r))
	}
	log.Infoln("Winners:", winners)
	log.Infoln("Multiplied:", slices_.Reduce(winners, builtin.Multiply[uint]))
	therace := slices_.Reduce(races, func(r1, r2 race) race {
		var timebuff []byte
		timebuff = strconv.AppendUint(timebuff, uint64(r1.time), 10)
		timebuff = strconv.AppendUint(timebuff, uint64(r2.time), 10)
		var distbuff []byte
		distbuff = strconv.AppendUint(distbuff, uint64(r1.distance), 10)
		distbuff = strconv.AppendUint(distbuff, uint64(r2.distance), 10)
		return race{time: strconv_.MustParseUintDecimal[uint](string(timebuff)), distance: strconv_.MustParseUintDecimal[uint](string(distbuff))}
	})
	log.Debugln("Race:", therace)
	count := countWinners(&therace)
	log.Infoln("Count:", count)
}
